USE abi_fil_rouge;

drop trigger if exists society_turnover_is_valid;
drop trigger if exists does_project_have_documents;
drop trigger if exists project_valuedStartDate_is_valid;
drop trigger if exists update_agreementType_is_valid;
drop trigger if exists dont_delete_if_project_have_less_than_2months;

/* CONSIGNE 9 */
/*==============================================================*/
DELIMITER $$
CREATE TRIGGER society_turnover_is_valid
before insert
on society for each row 
begin 
	if (new.turnover / new.headcount) > 1000000 then 
		signal sqlstate '45000' 
			set MESSAGE_TEXT = "The ratio between tunrover and heacount exceeds one million";
	end if;
end $$
DELIMITER ;

/* CONSIGNE 2 */
/*==============================================================*/
DELIMITER $$
CREATE TRIGGER does_project_have_documents
AFTER DELETE
ON project FOR EACH ROW
BEGIN
	DELETE FROM document d
	WHERE d.idDocument IN
		(SELECT concern.idDocument FROM concern WHERE concern.idProject = old.idProject); 
END$$
DELIMITER ;

/* CONSIGNE 8 */
/*==============================================================*/
DELIMITER $$
CREATE TRIGGER project_valuedStartDate_is_valid
BEFORE INSERT
ON project FOR EACH ROW
BEGIN
	if (new.valuedStartDate > new.valuedEndDate) then 
		signal sqlstate '45000' 
			set MESSAGE_TEXT = "The valued start date begins after the valued end date.";
	end if;
END$$
DELIMITER ;

/* CONSIGNE 10 */
/*==============================================================*/
DELIMITER $$
CREATE TRIGGER update_agreementType_is_valid
BEFORE INSERT
ON signedcontract FOR EACH ROW
BEGIN
	DECLARE oldAgreementType INT;
    select idAgreementType
      from signedcontract
     where idCoworker = NEW.idCoworker
      into oldAgreementType;
	IF (oldAgreementType = 3 AND NEW.idAgreementType = 3 ) THEN
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The coworker can't become an intern a second time.";
            
	elseif (oldAgreementType = 1 AND NEW.idAgreementType != 2 ) THEN
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The coworker can't become an intern after a temporary contract.";
            
	elseif (oldAgreementType = 2 AND NEW.idAgreementType != 2 ) THEN
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The coworker can't become an intern or have a temporary contract after a permanent contract.";
	end if;
END$$
DELIMITER ;

/* CONSIGNE 11 */
/*==============================================================*/
DELIMITER $$
CREATE TRIGGER dont_delete_if_project_have_less_than_2months
BEFORE DELETE
ON project FOR EACH ROW
BEGIN
	IF (old.realEndDate IS NULL) THEN 
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The project isn't closed, it can't be deleted";
	ELSEIF ( old.realEndDate > DATE_SUB(CURDATE(), INTERVAL 2 MONTH) ) THEN 
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The project can't be deleted because it has less than 2 months";
	ELSEIF ( old.realEndDate = '0000-00-00' ) THEN 
		SIGNAL SQLSTATE '45000' 
			set MESSAGE_TEXT = "The project can't be deleted because it has a compromised end date ";
	END IF;
END$$
DELIMITER ;
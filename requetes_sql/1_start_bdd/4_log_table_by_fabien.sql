USE abi_fil_rouge;

-- Basic tools to log message into the database
-- We may use them to log messages during a routine execution
-- First, a log table

create table if not exists `log` (
  -- The level is here purely indicative, it allows to filter the loglevel we
  -- might want to see with a select statement
  `level` int not null,
  
  -- A timestamp which defaults to the insetion time
  `timestamp` timestamp not null default current_timestamp(),
  
  -- The eventual message, could 255 be too short ?
  `message` varchar(255) not null
  
-- The MyISAM engine does not supports transaction and we use it to our 
-- advantage here as it should allow us to store logs even during an unfinished 
-- transaction
) engine=myisam; 

-- Second, some conveniency procedures

drop procedure if exists log_debug;
drop procedure if exists log_info;
drop procedure if exists log_warning;
drop procedure if exists log_error;
​
delimiter //

create procedure log_debug (message varchar(255))
begin
    insert into `log` (`level`, `message`) values (0, message);
end//

create procedure log_info (message varchar(255))
begin
    insert into `log` (`level`, `message`) values (1, message);
end//

create procedure log_warning (message varchar(255))
begin
    insert into `log` (`level`, `message`) values (2, message);
end//

create procedure log_error (message varchar(255))
begin
    insert into `log` (`level`, `message`) values (3, message);
end//
​
delimiter ;


-- Examples
call log_debug("A debug log");
call log_info("An info log message");
call log_warning("A warning log message");
call log_error("An error log message");

-- Let's see message aboce the debug level
select * from log where level > 0;
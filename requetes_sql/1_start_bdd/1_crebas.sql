/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  11/02/2020 09:16:08                      */
/*==============================================================*/
USE abi_fil_rouge;

drop table if exists intervention;
drop table if exists step;
drop table if exists concern;
drop table if exists author;
drop table if exists document;
drop table if exists `client`;
drop table if exists toolByProject;
drop table if exists project;
drop table if exists projectType;
drop table if exists cycle;
drop table if exists tool;
drop table if exists society;
drop table if exists societyType;
drop table if exists societyNature;
drop table if exists sector;
drop table if exists salaryHistory;
drop table if exists positionHistory;
drop table if exists signedContract;
drop table if exists acl;
drop table if exists coworker;
drop table if exists position;
drop table if exists privilege;
drop table if exists personalData;
drop table if exists agreementType;
drop table if exists salary;

/*==============================================================*/
/* Table : salary                                               */
/*==============================================================*/
create table salary
(
   idSalary             serial,
   salary               bigint not null,
   primary key (idSalary)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : agreementType                                        */
/*==============================================================*/
create table agreementType
(
   idAgreementType      serial,
   shortcode            varchar(3) not null unique,
   `description`        varchar(50) not null,
   primary key (idAgreementType)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : personalData                                         */
/*==============================================================*/
create table personalData
(
   idPersonalData       serial,
   shortcode            varchar(3) not null unique,
   sex                  char(1) not null,
   `description`        varchar(50) not null,
   primary key (idPersonalData)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : privilege                                            */
/*==============================================================*/
create table privilege
(
   `key`                  varchar(50) not null,
   `description`          varchar(50),
   primary key (`key`)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : position                                             */
/*==============================================================*/
create table position
(
   idPosition           serial,
   `description`        varchar(50) not null,
   primary key (idPosition)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : coworker                                             */
/*==============================================================*/
create table coworker
(
   idCoworker           serial,
   idPersonalData       BIGINT UNSIGNED NOT NULL,
   register             char(4) not null unique,
   fullname             varchar(250) not null,
   address1             varchar(250),
   address2             varchar(250),
   city                 varchar(250),
   postcode             char(5),
   phoneNumber          varchar(15) not null,
   login                varchar(10),
   `password`           char(8),
   isInternal           bool not null,
   primary key (idCoworker)
)ENGINE = INNODB;
alter table coworker add constraint FK_coworker_personalData_idPersonalData foreign key (idPersonalData)
      references personalData (idPersonalData) on delete restrict on update restrict;
      
/*==============================================================*/
/* Table : acl                                                  */
/*==============================================================*/
create table acl
(
   idCoworker           BIGINT UNSIGNED NOT NULL,
   `key`                varchar(50) not null,
   primary key (idCoworker, `key`)
)ENGINE = INNODB;
alter table acl add constraint FK_acl_coworker_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

alter table acl add constraint FK_acl_privilege_key foreign key (`key`)
      references privilege (`key`) on delete restrict on update restrict;

/*==============================================================*/
/* Table : signedContract                                       */
/*==============================================================*/
create table signedContract
(
   idCoworker           BIGINT UNSIGNED NOT NULL,
   idAgreementType      BIGINT UNSIGNED NOT NULL,
   agreementStartDate   date not null,
   agreementEndDate     date,
   primary key (idCoworker, idAgreementType, agreementStartDate)
)ENGINE = INNODB;
alter table signedContract add constraint FK_signedContract_coworker_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

alter table signedContract add constraint FK_signedContract_agreementType_idAgreementType foreign key (idAgreementType)
      references agreementType (idAgreementType) on delete restrict on update restrict;

/*==============================================================*/
/* Table : positionHistory                                      */
/*==============================================================*/
create table positionHistory
(
   idPosition           BIGINT UNSIGNED NOT NULL,
   idCoworker           BIGINT UNSIGNED NOT NULL,
   positionStartDate    date not null,
   primary key (idPosition, idCoworker, positionStartDate)
)ENGINE = INNODB;
alter table positionHistory add constraint FK_positionHistory_position_idPosition foreign key (idPosition)
      references position (idPosition) on delete restrict on update restrict;

alter table positionHistory add constraint FK_positionHistory_coworker_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

/*==============================================================*/
/* Table : salaryHistory                                        */
/*==============================================================*/
create table salaryHistory
(
   idCoworker           BIGINT UNSIGNED NOT NULL,
   idSalary             BIGINT UNSIGNED NOT NULL,
   earnDate             date not null,
   earnCoefficient      float not null,
   primary key (idCoworker, idSalary, earnDate)
)ENGINE = INNODB;

alter table salaryHistory add constraint FK_salaryHistory_coworker_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

alter table salaryHistory add constraint FK_salaryHistory_salary_idSalary foreign key (idSalary)
      references salary (idSalary) on delete restrict on update restrict;

/*==============================================================*/
/* Table : sector                                               */
/*==============================================================*/
create table sector
(
   idSector             serial,
   `description`        varchar(50) not null,
   primary key (idSector)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : societyNature                                        */
/*==============================================================*/
create table societyNature
(
   idNature             serial,
   shortcode            varchar(3) not null unique,
   `description`        varchar(50) not null,
   primary key (idNature)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : societyType                                          */
/*==============================================================*/
create table societyType
(
   idType               serial,
   shortcode            varchar(3) not null unique,
   `description`        varchar(50) not null,
   primary key (idType)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : society                                              */
/*==============================================================*/
create table society
(
   idSociety            serial,
   idNature             BIGINT UNSIGNED NOT NULL,
   idSector             BIGINT UNSIGNED NOT NULL,
   idType               BIGINT UNSIGNED NOT NULL,
   `number`             char(4) not null unique,
   businessName         varchar(250) not null,
   adress1              varchar(250) not null,
   adress2              varchar(250),
   city                 varchar(250) not null,
   postcode             char(5) not null,
   phoneNumber          varchar(15) not null,
   turnover             numeric(8,0),
   headcount            numeric(8,0),
   commentary           longtext,
   primary key (idSociety)
)ENGINE = INNODB;
alter table society add constraint FK_society_societyType_idType foreign key (idType)
      references societyType (idType) on delete restrict on update restrict;

alter table society add constraint FK_society_societyNature_idNature foreign key (idNature)
      references societyNature (idNature) on delete restrict on update restrict;

alter table society add constraint FK_society_sector_idSector foreign key (idSector)
      references sector (idSector) on delete restrict on update restrict;

/*==============================================================*/
/* Table : tool                                                 */
/*==============================================================*/
create table tool
(
   idTool               serial,
   `description`        varchar(50) not null,
   primary key (idTool)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : cycle                                                */
/*==============================================================*/
create table cycle
(
   idCycle              serial,
   `description`        varchar(50) not null unique,
   primary key (idCycle)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : projectType                                          */
/*==============================================================*/
create table projectType
(
   idProjectType        serial,
   shortcode            varchar(3) not null unique,
   `description`        varchar(50) not null,
   primary key (idProjectType)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : project                                              */
/*==============================================================*/
create table project
(
   idProject            serial,
   idProjectType        BIGINT UNSIGNED NOT NULL,
   idCycle              BIGINT UNSIGNED NOT NULL,
   idSociety            BIGINT UNSIGNED NOT NULL,
   codeProject          varchar(5) not null,
   shortDescription     varchar(10) not null,
   longDescription      varchar(250) not null,
   valuedStartDate      date not null,
   valuedEndDate        date not null,
   realStartDate        date,
   realEndDate          date,
   coworkerMaxNumber    int,
   valuedCommentary     longtext,
   technicalInformation longtext,
   primary key (idProject)
)ENGINE = INNODB;
alter table project add constraint FK_project_projectType_idProjectType foreign key (idProjectType)
      references projectType (idProjectType) on delete restrict on update restrict;

alter table project add constraint FK_project_cycle_idCycle foreign key (idCycle)
      references cycle (idCycle) on delete restrict on update restrict;

alter table project add constraint FK_project_society_idSociety foreign key (idSociety)
      references society (idSociety) on delete restrict on update restrict;

/*==============================================================*/
/* Table : client                                               */
/*==============================================================*/
create table `client`
(
   idClient             serial,
   idProject            BIGINT UNSIGNED NOT NULL,
   fullname             varchar(250) not null,
   phoneNumber          varchar(15) not null,
   primary key (idClient)
)ENGINE = INNODB;
alter table `client` add constraint FK_contact_project_idProject foreign key (idProject)
      references project (idProject) on delete cascade on update cascade;

/*==============================================================*/
/* Table : toolByProject                                        */
/*==============================================================*/
create table toolByProject
(
   idTool               BIGINT UNSIGNED NOT NULL,
   idProject            BIGINT UNSIGNED NOT NULL,
   primary key (idTool, idProject)
)ENGINE = INNODB;
alter table toolByProject add constraint FK_toolByProject_tool_idTool foreign key (idTool)
      references tool (idTool) on delete restrict on update restrict;

alter table toolByProject add constraint FK_toolByProject_poject_idProject foreign key (idProject)
      references project (idProject) on delete cascade on update cascade;

/*==============================================================*/
/* Table : document                                             */
/*==============================================================*/
create table document
(
   idDocument           serial,
   title                varchar(250) not null,
   summary              longtext not null,
   diffusionDate        date not null,
   primary key (idDocument)
)ENGINE = INNODB;

/*==============================================================*/
/* Table : author                                               */
/*==============================================================*/
create table author
(
   idDocument           BIGINT UNSIGNED NOT NULL,
   idCoworker           BIGINT UNSIGNED NOT NULL,
   primary key (idDocument, idCoworker)
)ENGINE = INNODB;
alter table author add constraint FK_author_document_idDocument foreign key (idDocument)
      references document (idDocument) on delete cascade on update cascade;

alter table author add constraint FK_author_document_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

/*==============================================================*/
/* Table : concern                                              */
/*==============================================================*/
create table concern
(
   idProject            BIGINT UNSIGNED NOT NULL,
   idDocument           BIGINT UNSIGNED NOT NULL,
   primary key (idProject, idDocument)
)ENGINE = INNODB;
alter table concern add constraint FK_concern_project_idProject foreign key (idProject)
      references project (idProject) on delete cascade on update cascade;

alter table concern add constraint FK_concern_document_idDocument foreign key (idDocument)
      references document (idDocument) on delete restrict on update restrict;

/*==============================================================*/
/* Table : step                                                 */
/*==============================================================*/
create table step
(
   idStep               serial,
   idProject            BIGINT UNSIGNED NOT NULL,
   lot                  char(6) not null unique,
   technicalInformation longtext,
   `description`        varchar(200) not null,
   primary key (idStep)
)ENGINE = INNODB;
alter table step add constraint FK_step_project_idProject foreign key (idProject)
      references project (idProject) on delete cascade on update cascade;
      
/*==============================================================*/
/* Table : intervention                                         */
/*==============================================================*/
create table intervention
(
   idIntervention       serial,
   idPosition           BIGINT UNSIGNED NOT NULL,
   idStep               BIGINT UNSIGNED NOT NULL,
   idCoworker           BIGINT UNSIGNED NOT NULL,
   `number`             char(4) not null unique,
   startDate            date not null,
   endDate              date,
   valuedCharge         int,
   validationCharge     int,
   productionCharge     int,
   technicalInformation longtext,
   primary key (idIntervention)
)ENGINE = INNODB;
alter table intervention add constraint FK_intervention_step_idStep foreign key (idStep)
      references step (idStep) on delete cascade on update cascade;

alter table intervention add constraint FK_intervention_coworker_idCoworker foreign key (idCoworker)
      references coworker (idCoworker) on delete restrict on update restrict;

alter table intervention add constraint FK_intervention_coworker_idPosition foreign key (idPosition)
      references position (idPosition) on delete restrict on update restrict;

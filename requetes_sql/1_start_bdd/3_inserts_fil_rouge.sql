USE abi_fil_rouge;

INSERT INTO `salary` (`salary`) VALUES 
	('590100'),
	('226200'),
	('1244800'),
	('230000'),
	('266900'),
	('213000'),
	('170700'),
	('221500'),
	('186200'),
	('273400'),
	('238600'),
	('203900'),
	('227800'),
	('237000'),
	('731700'),
	('328200'),
	('697700'),
	('318300'),
	('241500'),
	('303100'),
	('241900'),
	('279200'),
	('1020900'),
	('315500');

INSERT INTO `agreementType` (`shortcode`, `description`) VALUES 
	('CDD', 'Contrat à durée indéterminée'),
	('CDI', 'Contrat à durée déterminée'),
	('STA', 'Contrat de stage');

INSERT INTO `personaldata` (`shortcode`,sex, `description`) VALUES
	('MLE','F','Mademoiselle'),
    ('MME', 'F', 'Madame'),
    ('MR', 'M', 'Monsieur');

INSERT INTO privilege (`key`, `description`) VALUES
	('CREATE PROJECT','Autorisé à créer un projet'),
    ('UPDATE PROJECT','Autorisé à modifier un projet'),
    ('READ PROJECT','Autorisé à lire un projet'),
    ('DELETE PROJECT','Autorisé à supprimer un projet');
    
INSERT INTO position (`description`) VALUES
	('Directeur général'),
    ('Responsable des ressources humaines'),
    ('Responsable commercial'),
    ('Secrétaire administrative'),
    ("Responsable du département d'études"),
    ('Chef de projet'),
    ('Analyste'),
    ('Développeur'),
    ('Secrétaire technique'),
    ('Technicien support'),
    ('Commercial');

INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","9499","Kessie Boyle","Ap #120-2245 Posuere St.","Freire","67839","01 02 96 74 63","K.","jBf63U1U","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","7636","Mona Blackburn","P.O. Box 919, 9655 Diam St.","Gilly","15434","06 58 72 85 06","M.","uLT94M9o","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","7305","Aristotle Byrd","P.O. Box 629, 9278 Etiam Rd.","Zamora","93084","05 64 54 23 03","A.","vuV85w3E","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","9127","Uma Hood","P.O. Box 288, 9034 Enim St.","College","66627","04 60 80 42 45","U.","onj63K1H","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","6010","Nathan Nichols","P.O. Box 683, 8192 Proin Avenue","Hawera","27787","01 03 41 51 03","N.","icE21o9r","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","3247","Ayanna Torres","248-7001 Ornare. Rd.","Birmingham","23685","02 96 59 27 74","A.","zAn61e4S","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","7009","Julian Griffin","822-5841 Arcu. Road","San Benedetto del Tronto","40775","03 29 69 32 76","J.","Ftl91G8O","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8812","Blaze Gaines","Ap #298-9972 Vitae, St.","Hamilton","89112","03 43 47 11 52","B.","lFB95W4F","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","1836","Natalie Simpson","225-3274 Tortor. St.","Vucht","35431","06 47 57 90 21","N.","Zhc98y2z","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","6062","Coby Buckley","8926 Aliquam Ave","Montpellier","35709","03 71 26 32 85","C.","ihw16C9i","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","9033","Sophia Decker","P.O. Box 824, 1379 Velit Rd.","Vandoies/Vintl","76628","07 24 51 04 88","S.","cOa01D2k","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","5269","Cheryl Acevedo","P.O. Box 487, 3532 Hymenaeos. St.","Vancouver","40357","08 33 79 61 03","C.","XWb98R1J","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","1337","Jacob Curry","7245 Luctus Road","Reading","96401","02 82 76 82 37","J.","Nac08v3j","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","6057","Veda Frederick","185-5843 Facilisis Ave","Panguipulli","68164","06 61 02 55 11","V.","LkB94t2f","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","1423","Belle Holt","441-4214 Faucibus Av.","Duns","34382","09 99 68 91 91","B.","lWE17m1g","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","5576","Fatima Henson","7032 Nulla Av.","Castello di Godego","20528","09 81 69 52 05","F.","bgL40L8a","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","1573","Carol Keller","Ap #293-1216 Maecenas Av.","Brindisi","60546","04 86 11 44 02","C.","myL43V1Z","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3177","Oscar Hendrix","8519 Ultricies Av.","Karapınar","91392","09 59 64 33 08","O.","BZV09Q9K","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3096","Amir Sampson","Ap #624-7649 Elementum, Av.","Abbotsford","47237","01 05 32 56 95","A.","cdl08o6Y","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","2274","Unity Mcintosh","P.O. Box 361, 3412 Non, Rd.","South Portland","78919","04 30 99 47 24","U.","aTK87w5d","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","8004","Hedwig Navarro","Ap #171-1447 Augue Road","Borno","10593","09 81 21 89 51","H.","mjT86K8Q","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3727","Leroy Gross","Ap #572-6268 Lorem Road","New Orleans","60808","08 12 04 27 71","L.","hUy13T6q","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","5157","Stephen Madden","3929 Ante St.","Cabano","17565","04 48 90 83 58","S.","yFY62V9g","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4704","Abraham Chen","Ap #206-6630 Ridiculus Av.","Hamme","31474","06 37 80 53 27","A.","LKy95M6t","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4863","Honorato Gross","2544 Pretium St.","Broxburn","35486","02 55 48 34 06","H.","wnr81s8v","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","8815","MacKenzie Atkinson","318-4719 Ac Av.","Saint Petersburg","38618","09 29 47 17 71","M.","PqH79t4k","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","5044","Emi Mcbride","350-8887 Non, St.","Adana","48982","06 19 52 32 07","E.","xzY16A8d","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8184","Zahir Richmond","2293 Proin Rd.","Bay Roberts","29204","01 92 13 83 61","Z.","YnI33q8q","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","2817","Tanek Curtis","750-1612 Eget Av.","Nîmes","37615","01 54 49 22 67","T.","ceK52Y8A","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4247","Wallace Peck","844-8293 Proin Avenue","Buin","67561","09 40 17 37 98","W.","YDw72x8t","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","6604","Hollee Suarez","111-9479 In Rd.","Burntisland","50694","08 03 20 23 99","H.","Fan46x1o","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","0947","Laith Silva","Ap #446-6676 Egestas. Ave","Serampore","39646","01 30 32 68 63","L.","UML94h8Q","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3800","Owen Villarreal","2699 Lorem, Road","Picinisco","97671","05 68 51 28 29","O.","HfR43v2Q","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","0834","Maxine Anderson","749-1715 Sed Rd.","Dera Ismail Khan","53716","06 80 18 47 92","M.","jgM51D3Z","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8575","Louis Preston","Ap #394-2066 Tempus Street","Kent","53479","05 02 84 41 03","L.","EWl45s6i","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","5158","Ima French","4190 Parturient St.","Travo","74773","03 99 35 78 65","I.","OoN79W6H","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","4546","Kelsie Mcclure","7317 Aliquet Av.","Petrópolis","90539","01 09 15 00 76","K.","jyG23d7B","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","9277","Yardley Alvarez","2140 Convallis St.","Minyar","18204","06 54 22 80 13","Y.","tMx05B7Q","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","4075","Hayfa Turner","P.O. Box 992, 4784 Id Ave","Beauport","91673","06 96 60 27 90","H.","FJk12W1K","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","6138","Kadeem Mccarty","Ap #494-819 Ligula Rd.","Delta","46725","02 69 57 38 82","K.","yre66m9J","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","8870","Sarah Robbins","5290 Quis, Avenue","Lloydminster","14955","06 92 32 28 85","S.","wpi54T6b","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","5633","Rose Cleveland","P.O. Box 757, 3123 Convallis St.","Socchieve","78478","02 06 59 68 95","R.","KVp86R1s","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","2196","Sebastian Moody","Ap #818-1540 Lorem St.","Saint-Remy","64208","08 30 40 18 72","S.","BJm23h7C","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","2073","Cole Baird","Ap #505-5438 Amet, Rd.","Merchtem","47866","05 95 78 25 21","C.","biK25B1e","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","1943","Leandra Henson","573-4079 Massa. Rd.","Milwaukee","64855","05 99 92 58 15","L.","cfi11N9S","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","5244","Zeus Kane","181 Euismod Road","Roccalumera","10488","07 42 26 71 66","Z.","iAz41o7T","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","6083","Portia Stark","P.O. Box 774, 4345 Volutpat Av.","Poviglio","09839","07 25 03 78 02","P.","Jiv51P6U","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","9993","Susan Vazquez","7879 Libero. Avenue","Lauder","47470","08 65 98 25 77","S.","DWr43z6p","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8597","Kirk Blevins","617-3568 Sit Road","Aieta","55488","08 49 18 19 46","K.","xxQ70U3g","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4333","Hoyt King","P.O. Box 895, 5371 Egestas St.","Pontboset","25850","05 18 27 80 40","H.","BxC21u9e","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3615","Chaim Andrews","Ap #180-6757 Proin Street","Armadale","45802","05 59 97 11 41","C.","XVX32t3A","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","9037","Sophia Mccullough","619-6289 Proin Avenue","Notre-Dame-du-Nord","39049","03 61 36 75 83","S.","rYd89S9w","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","9449","Audra Sullivan","Ap #131-5249 Dictum St.","Montague","14226","05 02 85 05 37","A.","UHC79r5d","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","7998","Avye Schneider","304-1596 Curabitur Rd.","Amelia","68259","03 13 26 19 28","A.","kTi60a8N","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","6238","Rhoda Frost","556-7216 Quam. Ave","Liers","23230","02 18 76 52 84","R.","giP40w1y","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","5678","Leah Durham","Ap #706-7518 Magnis Avenue","Salzburg","02036","07 40 05 34 34","L.","YFv50l7u","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4830","Rogan Levine","686-347 Enim Avenue","Angol","28905","03 75 37 84 61","R.","vmI52d1w","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","8757","Oprah Arnold","680-2536 Nunc St.","Curepto","73328","09 33 34 86 82","O.","OFb77h4v","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","1255","Medge Pena","P.O. Box 675, 7898 Odio Road","Corswarem","78624","05 08 79 11 86","M.","CLj90M9l","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","4493","Belle Mullen","P.O. Box 299, 6039 Elementum, Avenue","Inverurie","39762","05 21 14 53 95","B.","VWB37p9P","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","3366","Eve Baxter","P.O. Box 276, 286 Quis, Ave","Perth","15896","09 94 15 45 94","E.","srP67t8X","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","1814","Lavinia Ortega","7805 Nisi. Road","Uddevalla","41798","05 19 01 11 77","L.","Pxh84B2O","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","1782","Mari Heath","P.O. Box 397, 5714 Sollicitudin Rd.","Lozzo Atestino","86384","06 29 55 08 11","M.","ZCg32P2c","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","0287","Drake Olson","P.O. Box 241, 3586 Est Street","Wells","62776","06 56 14 15 19","D.","WPV83V2g","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","2898","Steel Miles","948-4385 Lectus Avenue","Chile Chico","53572","02 87 04 62 09","S.","PUf29w6N","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","6241","Felix Chandler","P.O. Box 861, 3513 Amet St.","Parla","38824","01 03 37 42 30","F.","kyV09C6q","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","5785","Colt Ashley","843 Non Road","Pinto","35243","04 13 47 19 55","C.","odB54j3V","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","9776","Driscoll Dillard","Ap #253-5772 Donec St.","The Hague","57215","02 22 15 72 47","D.","rSH11N5d","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8871","Alec House","Ap #385-9775 Aliquam Ave","Henderson","12611","08 48 53 75 17","A.","smv62K3K","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","1331","Kirk Mullen","278-5535 Nec Rd.","Freising","99815","02 81 94 65 78","K.","eTK76I8e","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4091","Lucian Giles","Ap #538-4032 Lorem St.","Herne","66890","02 02 11 01 09","L.","wbs80b8P","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","7539","Fallon Foreman","P.O. Box 485, 9540 Proin Rd.","Bilaspur","55259","02 28 38 86 12","F.","Kko55D9F","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","0405","Samuel Wilkins","7674 Aliquam, St.","Lleida","12013","01 46 51 87 57","S.","awl95w8u","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","6798","Lacota Bender","Ap #450-6175 Ut Avenue","Paranaguá","65686","03 28 41 28 65","L.","XAX18m2L","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","5733","Genevieve Snider","P.O. Box 213, 9253 Orci Rd.","Pucón","64187","02 56 95 48 49","G.","IfN35d9W","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","6021","Maris Graham","1769 Enim St.","Gallodoro","46357","06 34 64 83 63","M.","fMN02V8j","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","8384","Jayme Casey","9436 Massa Road","Hassan","66659","08 04 33 28 21","J.","Cau10b3B","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","8965","Colleen Mayo","6384 Lacinia Road","Squillace","11287","06 68 76 67 92","C.","QPK41e9m","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","7396","Veda Forbes","448 Magna Street","Lansing","91973","01 92 73 75 17","V.","Zzo97c2N","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","0808","Sean Kelly","788-8784 Consequat St.","Howrah","27286","01 16 76 23 62","S.","LTM23j1G","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","5006","Reuben Mckenzie","2771 Fringilla. Road","Whitehorse","11130","01 07 39 74 95","R.","qNV04c8w","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","1320","Kamal Wise","P.O. Box 478, 9450 Magna. Ave","Independence","83149","09 09 42 95 95","K.","pBy50c5f","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3945","Jelani Perez","P.O. Box 498, 1549 Ultrices, Ave","Balfour","94601","04 48 70 88 21","J.","MQl67S9F","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4801","Lucius Lambert","592-9898 Augue Rd.","York","41961","05 74 16 01 98","L.","tud66L7w","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","2572","Iliana Norton","423-6701 Non, St.","Monteleone di Spoleto","59631","07 55 69 21 79","I.","VSr33g7d","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","2022","Hadley Mcgowan","576-6903 Inceptos St.","Darbhanga","47038","08 68 31 49 50","H.","Ogt68c9B","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","5933","Linus Delacruz","453-9127 Magna. Street","Brandon","05221","09 40 87 84 76","L.","Uqy01k8e","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4380","Cairo Mathews","Ap #862-5788 Donec Rd.","Veldwezelt","32678","01 95 61 28 32","C.","ioU70x2m","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","0321","Kyra Barlow","Ap #593-7647 Eu, Ave","Russell","64554","05 32 39 73 53","K.","TOP72G9G","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","9916","Lani Jacobs","Ap #818-3673 Quis St.","Bridgend","86921","09 09 93 28 47","L.","zAc29W4A","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","8652","Colt Poole","P.O. Box 283, 5774 Eu Road","Bida","68717","05 90 12 61 27","C.","CmB15T2x","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","9063","Nerea Freeman","P.O. Box 159, 2887 Semper Rd.","Gimcheon","05713","04 35 70 92 29","N.","LXb92A9T","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","4881","Valentine Campbell","7365 Quis, St.","Canning","60063","07 64 09 82 21","V.","fNI67Y1v","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","6519","Camilla Sargent","8057 Donec Street","Tsiigehtchic","33957","05 39 35 24 68","C.","BLz11h4G","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","0548","Lillith Yates","9732 Proin Rd.","Tuktoyaktuk","19053","08 88 29 23 42","L.","UDr55T4K","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","0005","Shaeleigh Hardy","812-2465 Aliquam Road","Rödermark","85102","02 03 11 74 97","S.","XMv74U3m","0");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","4602","Francesca Mathews","500-5046 Turpis. Rd.","Giove","83657","09 64 18 71 37","F.","XBA41k4r","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 3","3642","Victor Carson","9756 Erat Road","Cles","88044","01 48 41 58 46","V.","kcH69D5E","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 1","6613","Jane Hunter","2987 Habitant Av.","Dudley","47581","08 91 14 43 36","J.","sPi80c6d","1");
INSERT INTO `coworker` (`idPersonalData`,`register`,`fullname`,`address1`,`city`,`postcode`,`phoneNumber`,`login`,`password`,`isInternal`) VALUES (" 2","9376","Selma Norton","Ap #774-6339 Mi. Road","North Vancouver","69598","05 92 56 50 16","S.","tSC53S1X","1");

INSERT INTO `signedcontract` (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(35,3,'2016-07-14','2021-10-18'),
	(36,3,'2017-08-10','2021-06-08'),
	(37,3,'2019-08-06','2019-08-04'),
	(38,1,'2020-01-07','2020-01-06'),
	(39,1,'2017-10-25','2019-07-07'),
	(40,3,'2019-08-06','2019-08-04'),
	(41,3,'2016-10-13','2020-09-06'),
	(42,2,'2018-05-16',null),
	(43,1,'2019-12-30','2019-05-09'),
	(44,2,'2015-04-22',null),
	(45,1,'2017-12-23','2019-12-24'),
	(46,2,'2015-04-22',null),
	(47,1,'2017-03-20','2020-03-20'),
	(48,1,'2019-04-03','2020-09-14'),
	(49,1,'2017-03-10','2021-07-29'),
	(50,1,'2017-09-02','2020-03-26'),
	(51,3,'2018-06-06','2019-02-17'),
	(52,1,'2017-03-10','2021-07-29'),
	(53,2,'2018-12-16',null),
	(54,3,'2017-08-10','2021-06-08'),
	(55,1,'2017-03-20','2020-03-20'),
	(56,1,'2019-04-03','2020-09-14'),
	(57,2,'2016-08-20',null),
	(58,2,'2015-05-18',null),
	(59,1,'2019-11-06','2021-04-03'),
	(60,1,'2017-09-02','2020-03-26'),
	(61,2,'2018-03-13',null),
	(62,3,'2016-03-19','2020-06-05'),
	(63,2,'2016-04-07',null),
	(64,1,'2016-03-18','2019-06-26'),
	(65,1,'2016-05-02','2020-07-05'),
	(66,3,'2016-03-02','2020-11-08'),
	(67,1,'2015-04-06','2021-12-23'),
	(68,1,'2016-03-18','2019-06-26'),
	(69,3,'2018-02-03','2019-04-27'),
	(70,2,'2018-03-13',null),
	(71,1,'2015-11-18','2021-11-27'),
	(72,3,'2019-08-26','2021-05-10'),
	(73,3,'2019-08-26','2021-05-10'),
	(74,2,'2018-03-13',null),
	(75,3,'2015-01-21','2020-01-27'),
	(76,2,'2017-10-09',null),
	(77,2,'2017-10-09',null),
	(78,1,'2016-05-02','2020-07-05'),
	(79,1,'2018-02-01','2020-01-01'),
	(80,3,'2015-01-25','2019-12-24'),
	(81,3,'2018-02-03','2019-04-27'),
	(82,3,'2018-06-23','2022-01-29'),
	(83,3,'2016-10-13','2020-08-24'),
	(84,3,'2020-01-16','2021-04-27'),
	(85,1,'2019-10-15','2020-12-01'),
	(86,2,'2017-10-09',null),
	(87,3,'2020-01-16','2021-04-27'),
	(88,1,'2018-07-22','2019-04-15'),
	(89,2,'2016-05-24',null),
	(90,2,'2018-09-02',null),
	(91,1,'2019-11-16','2020-09-03'),
	(92,1,'2015-08-10','2020-08-29'),
	(93,2,'2016-06-19',null),
	(94,2,'2017-06-07',null),
	(95,3,'2018-02-09','2019-11-11'),
	(96,2,'2016-06-19',null),
	(97,2,'2020-02-07',null),
	(98,2,'2016-03-07',null),
	(99,1,'2019-03-19','2019-10-30'),
	(100,1,'2018-02-01','2020-01-01');

INSERT INTO `signedcontract` (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(1,2,'2016-02-19',null),
	(2,2,'2016-02-19',null),
	(3,2,'2016-02-19',null),
	(4,2,'2016-02-19',null),
	(5,2,'2016-02-19',null),
	(6,2,'2016-02-19',null),
	(7,2,'2016-02-19',null),
	(8,2,'2016-02-19',null),
	(9,2,'2016-02-19',null),
	(10,2,'2016-02-19',null),
	(11,2,'2016-02-19',null),
	(12,2,'2016-02-19',null),
	(13,2,'2016-02-19',null),
	(14,2,'2016-02-19',null),
	(15,2,'2016-02-19',null),
	(16,2,'2016-02-19',null),
	(17,2,'2016-02-19',null),
	(18,2,'2016-02-19',null),
	(19,2,'2016-02-19',null),
	(20,2,'2016-02-19',null),
	(21,2,'2016-02-19',null),
	(22,2,'2016-02-19',null),
	(23,2,'2016-02-19',null),
	(24,2,'2016-02-19',null),
	(25,2,'2016-02-19',null),
	(26,2,'2016-02-19',null),
	(27,2,'2016-02-19',null),
	(28,2,'2016-02-19',null),
	(29,2,'2016-02-19',null),
	(30,2,'2016-02-19',null),
	(31,2,'2016-02-19',null),
	(32,2,'2016-02-19',null),
	(33,2,'2016-02-19',null),
	(34,2,'2016-02-19',null);

INSERT INTO acl (idCoworker,`key`) VALUES
	(1,'CREATE PROJECT'),(1,'DELETE PROJECT'),(1,'UPDATE PROJECT'),(1,'READ PROJECT'),
	(2,'CREATE PROJECT'),(2,'DELETE PROJECT'),(2,'UPDATE PROJECT'),(2,'READ PROJECT'),
	(3,'CREATE PROJECT'),(3,'DELETE PROJECT'),(3,'UPDATE PROJECT'),(3,'READ PROJECT'),
	(4,'CREATE PROJECT'),(4,'UPDATE PROJECT'),(4,'READ PROJECT'),
	(5,'CREATE PROJECT'),(5,'UPDATE PROJECT'),(5,'READ PROJECT'),
	(6,'CREATE PROJECT'),(6,'UPDATE PROJECT'),(6,'READ PROJECT'),
	(7,'CREATE PROJECT'),(7,'UPDATE PROJECT'),(7,'READ PROJECT'),
	(8,'CREATE PROJECT'),(8,'UPDATE PROJECT'),(8,'READ PROJECT'),
	(9,'CREATE PROJECT'),(9,'UPDATE PROJECT'),(9,'READ PROJECT'),
	(10,'UPDATE PROJECT'),(10,'READ PROJECT'),
	(11,'UPDATE PROJECT'),(11,'READ PROJECT'),
	(12,'UPDATE PROJECT'),(12,'READ PROJECT'),
	(13,'UPDATE PROJECT'),(13,'READ PROJECT'),
	(14,'UPDATE PROJECT'),(14,'READ PROJECT'),
	(15,'UPDATE PROJECT'),(15,'READ PROJECT'),
	(16,'UPDATE PROJECT'),(16,'READ PROJECT'),
	(17,'UPDATE PROJECT'),(17,'READ PROJECT'),
	(18,'UPDATE PROJECT'),(18,'READ PROJECT'),
	(19,'UPDATE PROJECT'),(19,'READ PROJECT'),
	(20,'UPDATE PROJECT'),(20,'READ PROJECT'),
	(21,'UPDATE PROJECT'),(21,'READ PROJECT'),
	(22,'UPDATE PROJECT'),(22,'READ PROJECT'),
	(23,'UPDATE PROJECT'),(23,'READ PROJECT'),
	(24,'UPDATE PROJECT'),(24,'READ PROJECT'),
	(25,'UPDATE PROJECT'),(25,'READ PROJECT'),
	(26,'UPDATE PROJECT'),(26,'READ PROJECT'),
	(27,'UPDATE PROJECT'),(27,'READ PROJECT'),
	(28,'UPDATE PROJECT'),(28,'READ PROJECT'),
	(29,'UPDATE PROJECT'),(29,'READ PROJECT'),
	(30,'UPDATE PROJECT'),(30,'READ PROJECT'),
	(31,'UPDATE PROJECT'),(31,'READ PROJECT'),
	(32,'UPDATE PROJECT'),(32,'READ PROJECT'),
	(33,'UPDATE PROJECT'),(33,'READ PROJECT'),
	(34,'UPDATE PROJECT'),(34,'READ PROJECT');

INSERT INTO positionhistory (idCoworker,idPosition, positionStartDate) VALUES
	(1,1,'2016-02-19'),
	(2,2,'2016-02-19'),
	(3,3,'2016-02-19'),
	(4,4,'2016-02-19'),
	(5,5,'2016-02-19'),
	(6,6,'2016-02-19'),
	(7,6,'2016-02-19'),
	(8,6,'2016-02-19'),
	(9,6,'2016-02-19'),
	(10,7,'2016-02-19'),
	(11,7,'2016-02-19'),
	(12,7,'2016-02-19'),
	(13,7,'2016-02-19'),
	(14,7,'2016-02-19'),
	(15,7,'2016-02-19'),
	(16,7,'2016-02-19'),
	(17,7,'2016-02-19'),
	(18,8,'2016-02-19'),
	(19,8,'2016-02-19'),
	(20,8,'2016-02-19'),
	(21,8,'2016-02-19'),
	(22,8,'2016-02-19'),
	(23,8,'2016-02-19'),
	(24,8,'2016-02-19'),
	(25,8,'2016-02-19'),
	(26,9,'2016-02-19'),
	(27,9,'2016-02-19'),
	(28,10,'2016-02-19'),
	(29,10,'2016-02-19'),
	(30,10,'2016-02-19'),
	(31,10,'2016-02-19'),
	(32,10,'2016-02-19'),
	(33,11,'2016-02-19'),
	(34,11,'2016-02-19'),
    (33,3,'2019-08-12'),
    (34,3,'2019-12-31'),
    (6,5,'2018-11-28'),
    (7,5,'2019-07-15');

INSERT INTO salaryhistory (idCoworker, earnDate, earnCoefficient, idSalary) VALUES
	(1,'2016-02-19','1',13),
	(2,'2016-02-19','1',21),
	(3,'2016-02-19','1',16),
	(4,'2016-02-19','1',24),
	(5,'2016-02-19','1',7),
	(6,'2016-02-19','1',1),
	(7,'2016-02-19','1',5),
	(8,'2016-02-19','1',15),
	(9,'2016-02-19','1',19),
	(10,'2016-02-19','1',3),
	(11,'2016-02-19','1',12),
	(12,'2016-02-19','1',16),
	(13,'2016-02-19','1',13),
	(14,'2016-02-19','1',15),
	(15,'2016-02-19','1',23),
	(16,'2016-02-19','1',21),
	(17,'2016-02-19','1',14),
	(18,'2016-02-19','1',8),
	(19,'2016-02-19','1',12),
	(20,'2016-02-19','1',9),
	(21,'2016-02-19','1',17),
	(22,'2016-02-19','1',13),
	(23,'2016-02-19','1',17),
	(24,'2016-02-19','1',7),
	(25,'2016-02-19','1',1),
	(26,'2016-02-19','1',11),
	(27,'2016-02-19','1',12),
	(28,'2016-02-19','1',15),
	(29,'2016-02-19','1',5),
	(30,'2016-02-19','1',5),
	(31,'2016-02-19','1',3),
	(32,'2016-02-19','1',19),
	(33,'2016-02-19','1',23),
	(34,'2016-02-19','1',16),
	(35,'2016-07-14','1',22),
	(36,'2017-08-10','1',11),
	(37,'2019-08-06','1',12),
	(38,'2020-01-07','1',1),
	(39,'2017-10-25','1',11),
	(40,'2019-08-06','1',20),
	(41,'2016-10-13','1',1),
	(42,'2018-05-16','1',5),
	(43,'2019-12-30','1',3),
	(44,'2015-04-22','1',13),
	(45,'2017-12-23','1',18),
	(46,'2015-04-22','1',16),
	(47,'2017-03-20','1',23),
	(48,'2019-04-03','1',10),
	(49,'2017-03-10','1',13),
	(50,'2017-09-02','1',11),
	(51,'2018-06-06','1',16),
	(52,'2017-03-10','1',3),
	(53,'2018-12-16','1',18),
	(54,'2017-08-10','1',10),
	(55,'2017-03-20','1',2),
	(56,'2019-04-03','1',1),
	(57,'2016-08-20','1',9),
	(58,'2015-05-18','1',17),
	(59,'2019-11-06','1',1),
	(60,'2017-09-02','1',9),
	(61,'2018-03-13','1',15),
	(62,'2016-03-19','1',2),
	(63,'2016-04-07','1',8),
	(64,'2016-03-18','1',10),
	(65,'2016-05-02','1',9),
	(66,'2016-03-02','1',13),
	(67,'2015-04-06','1',16),
	(68,'2016-03-18','1',15),
	(69,'2018-02-03','1',13),
	(70,'2018-03-13','1',19),
	(71,'2015-11-18','1',15),
	(72,'2019-08-26','1',21),
	(73,'2019-08-26','1',21),
	(74,'2018-03-13','1',10),
	(75,'2015-01-21','1',1),
	(76,'2017-10-09','1',2),
	(77,'2017-10-09','1',10),
	(78,'2016-05-02','1',22),
	(79,'2018-02-01','1',9),
	(80,'2015-01-25','1',13),
	(81,'2018-02-03','1',24),
	(82,'2018-06-23','1',9),
	(83,'2016-10-13','1',5),
	(84,'2020-01-16','1',9),
	(85,'2019-10-15','1',13),
	(86,'2017-10-09','1',8),
	(87,'2020-01-16','1',17),
	(88,'2018-07-22','1',20),
	(89,'2016-05-24','1',4),
	(90,'2018-09-02','1',17),
	(91,'2019-11-16','1',6),
	(92,'2015-08-10','1',24),
	(93,'2016-06-19','1',7),
	(94,'2017-06-07','1',12),
	(95,'2018-02-09','1',10),
	(96,'2016-06-19','1',14),
	(97,'2020-02-07','1',13),
	(98,'2016-03-07','1',23),
	(99,'2019-03-19','1',8),
	(100,'2018-02-01','1',9);

INSERT INTO sector (`description`) VALUES
	('Agroalimentaire'),
    ('Banque'),
    ('Assurance'),
    ('BTP'),
    ("Chime"),
    ('Commerce'),
    ('Edition'),
    ('Electricité');
    
INSERT INTO societynature (shortcode,`description`) VALUES
	('P','Principale'),
    ('S','Secondaire'),
    ('A','Ancienne');
    
INSERT INTO societytype (shortcode,`description`) VALUES
	('1','Public'),
    ('2','Privé');

INSERT INTO `society` (`idNature`,`idSector`,`idType`,`number`,`businessName`, `adress1`, `adress2`,`city`,`postcode`,`phoneNumber`,`turnover`,`headcount`) VALUES
	(2,1,1,"9700","Enim Condimentum Eget Industries","Appartement 402", "7750 Dictum Avenue","Lahore","98435","01 87 77 18 40",138188,880),
	(2,2,1,"5543","Posuere At Velit PC","Appartement 795", "1946 Risus. Avenue","Moignelee","64512","08 04 92 97 60",956494,401),
	(3,5,2,"8744","Odio Associates","", "6611 Massa. Ave","Spokane","47569","03 61 81 16 39",452101,547),
	(3,6,1,"3600","In Lobortis Tellus Ltd","Appartement 880", "9192 Dolor Route","Episcopia","56273","02 57 26 55 59",97194,753),
	(3,7,1,"2644","Libero Dui Nec Corporation","", "670 Fringilla. Avenue","Golspie","89227","01 60 45 23 45",848139,714),
	(2,7,2,"0735","Dui Augue Eu PC","", "489-8442 Turpis Ave","Perpignan","91119","07 73 15 03 73",450261,708),
	(2,3,1,"4905","Sapien Gravida PC","", "8814 Eget, Av.","Panjim","44255","04 86 40 53 62",209694,62),
	(1,2,2,"7680","Habitant Morbi Corp.","", "3819 Vel Rd.","Maracanaú","02006","03 53 87 38 55",26729,650),
	(1,3,1,"9736","Malesuada Inc.","Appartement 510", "7921 Donec Rd.","Southampton","10988","07 71 33 96 51",309894,497),
	(2,7,1,"0277","Ante Vivamus Non PC","CP 160", "7448 Diam. Route","Mamuju","34623","01 47 97 69 29",298420,824);
INSERT INTO `society` (`idNature`,`idSector`,`idType`,`number`,`businessName`,`adress1`, `adress2`,`city`,`postcode`,`phoneNumber`,`turnover`,`headcount`) VALUES 
	(3,4,1,"2564","Mollis LLC","Appartement 566", "986 In Impasse","Stockerau","15013","04 28 20 31 40",830218,36),
	(2,6,2,"5895","Commodo Auctor Company","", "3731 Eget, Rue","Melsbroek","41583","09 26 97 66 08",103890,315),
	(1,5,1,"6602","Dui In Sodales Institute","Appartement 964", "2982 Nullam Rue","Hoofddorp","44199","07 76 62 66 97",492914,675),
	(2,3,2,"1654","Neque Morbi Quis Corp.","CP 141", " 6575 Luctus, Impasse","Balashikha","97227","08 57 17 27 12",592791,644),
	(1,2,2,"4775","Fusce LLC","", "5196 Suspendisse Ave","Bischofshofen","24678","05 70 09 78 70",902976,631),
	(1,1,2,"6966","Purus LLC","Appartement 307", "9036 Et Av.","Morelia","29483","04 18 38 03 21",642752,213),
	(2,1,2,"0643","Proin Dolor Company","", "844-145 Quis Av.","Llangollen","23250","02 21 82 47 91",995025,799),
	(1,8,2,"7150","Consectetuer Consulting","", "1772 Egestas Chemin","Amqui","28058","06 86 64 25 39",322930,432),
	(1,6,1,"2080","Proin Dolor Corp.","", "3518 Curabitur Chemin","Changwon","12163","08 50 68 64 03",213688,834),
	(3,7,1,"5345","Facilisis Magna Tellus Foundation","", "996 Aliquet. Chemin","Cochrane","34510","04 80 45 37 56",875877,266);

INSERT INTO tool (`description`) VALUES
	('PHP'),
    ('Synfony'),
    ('Laravel'),
    ('HTML'),
    ('CSS'),
    ('Wordpress'),
    ('Javascript'),
    ('Angular'),
    ('React JS'),
    ('SASS'),
    ('Vue JS'),
    ('NodeJS'),
    ('Ruby'),
    ('Ruby on Rails'),
    ('Cordova'),
    ('React Native'),
    ('Xamarin'),
    ('Objective-C'),
    ('C#'),
    ('C / C++'),
    ('Python'),
    ('SQL'),
    ('MySQL'),
    ('NoSQL'),
    ('PostgreSQL'),
    ('JAVA');

INSERT INTO cycle (`description`) VALUES
	('Complet'),
    ("Etude de l'existant"),
    ('Développement');

INSERT INTO projecttype (shortcode, `description`) VALUES
	('F','Forfait'),
    ('R','Régie'),
    ('A','Assistance');

INSERT INTO project (idProject, idProjectType, idCycle, idSociety, codeProject, shortDescription, longDescription, valuedStartDate, valuedEndDate, realStartDate, realEndDate, coworkerMaxNumber, valuedCommentary, technicalInformation) VALUES
	('1', '2', '2', '1', '1702', 'inter', 'venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien', '2017-10-09', '2018-04-09', '2017-10-15', '2018-04-15', '16', 'enim sit amet nunc viverra dapibus nulla suscipit ligula', 'in porttitor pede justo eu massa donec'),
	('2', '2', '3', '2', '1802', 'donec', 'luctus et ultrices posuere cubilia curae nulla', '2018-08-10', '2019-02-10', '2018-08-16', '2019-02-16', '20', 'tortor duis mattis egestas metus aenean fermentum donec', 'aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate'),
	('3', '2', '2', '3', '1804', 'mauris', 'curabitur convallis duis consequat dui nec nisi volutpat eleifend', '2018-10-22', '2019-04-22', '2018-10-28', '2019-04-28', '14', 'lobortis ligula sit amet eleifend pede libero quis orci', 'commodo vulputate justo in blandit ultrices'),
	('4', '2', '2', '4', '1911', 'vulputate', 'aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas', '2019-10-29', '2020-04-29', '2019-11-04', null, '11', 'placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros', 'massa tempor convallis nulla neque libero convallis eget'),
	('5', '3', '2', '5', '2001', 'ac enim in', 'pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus', '2020-01-26', '2020-07-26', '2020-02-01', null, '19', 'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec', 'et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl'),
	('6', '2', '2', '6', '1909', 'id justo', 'ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris', '2019-10-25', '2020-04-25', '2019-10-31', null, '18', 'nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget', 'praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus'),
	('7', '2', '3', '7', '1903', 'rutrum', 'consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis', '2019-04-29', '2019-10-29', '2019-05-05', '2019-11-05', '11', 'amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus', 'quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa'),
	('8', '2', '2', '8', '1902', 'nunc comm', 'odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus', '2019-03-07', '2019-09-07', '2019-03-13', '2019-09-13', '13', 'fusce congue diam id ornare imperdiet sapien urna pretium nisl', 'sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris'),
	('9', '1', '1', '9', '1904', 'vivamus ve', 'aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2019-05-06', '2019-11-06', '2019-05-12', '2019-11-12', '10', 'ligula pellentesque ultrices phasellus id', 'id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy'),
	('10', '1', '1', '10', '1908', 'cursus id', 'integer non velit donec diam neque vestibulum eget vulputate', '2019-09-18', '2020-03-18', '2019-09-24', null, '15', 'aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer', 'in felis donec semper sapien'),
	('11', '2', '2', '11', '1907', 'dolor sit', 'ac tellus semper interdum mauris ullamcorper purus sit amet nulla', '2019-08-19', '2020-02-19', '2019-08-25', null, '19', 'parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum', 'vestibulum proin eu mi nulla ac enim'),
	('12', '2', '3', '12', '1906', 'ridiculus', 'tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut', '2019-08-09', '2020-02-09', '2019-08-15', '2020-02-15', '12', 'in felis donec semper sapien a libero nam dui proin leo odio', 'lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum'),
	('13', '3', '2', '13', '1910', 'rhoncus', 'justo in blandit ultrices enim lorem ipsum dolor', '2019-10-28', '2020-04-28', '2019-11-03', null, '13', 'faucibus orci luctus et ultrices posuere cubilia', 'justo eu massa donec dapibus duis at velit'),
	('14', '1', '2', '14', '1701', 'odio in', 'lacinia eget tincidunt eget tempus vel pede morbi porttitor', '2017-07-20', '2018-01-20', '2017-07-26', '2018-01-26', '6', 'non quam nec dui luctus rutrum', 'hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem'),
	('15', '3', '1', '15', '1905', 'velit viva', 'suspendisse potenti in eleifend quam a odio', '2019-06-06', '2019-12-06', '2019-06-12', '2019-12-12', '10', 'etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna', 'mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc'),
	('16', '2', '3', '16', '1913', 'nibh quis', 'curae nulla dapibus dolor vel est donec odio justo sollicitudin', '2019-12-23', '2020-06-23', '2019-12-29', null, '5', 'ut at dolor quis odio consequat varius integer ac', 'molestie nibh in lectus pellentesque at'),
	('17', '1', '2', '17', '1803', 'nisl nunc', 'mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent', '2018-10-15', '2019-04-15', '2018-10-21', '2019-04-21', '19', 'nec condimentum neque sapien placerat ante', 'duis bibendum morbi non quam nec dui luctus rutrum nulla tellus'),
	('18', '3', '3', '18', '1901', 'ut erat', 'faucibus accumsan odio curabitur convallis duis', '2018-12-27', '2019-06-27', '2019-01-02', '2019-07-02', '11', 'quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat', 'ut dolor morbi vel lectus in quam'),
	('19', '2', '2', '20', '1912', 'integer', 'sit amet diam in magna bibendum imperdiet nullam orci pede venenatis', '2019-11-13', '2020-05-13', '2019-11-19', null, '13', 'in lectus pellentesque at nulla suspendisse potenti cras in purus eu', 'orci luctus et ultrices posuere cubilia curae mauris viverra'),
	('20', '2', '2', '20', '1801', 'quis orci', 'vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget', '2018-02-08', '2018-08-08', '2018-02-14', '2018-08-14', '18', 'est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est', 'in blandit ultrices enim lorem');
    


INSERT INTO `client` (`idProject`,`fullname`,`phoneNumber`) VALUES (1,"Nathan Blanchard","07 04 26 34 29"),
(2,"Robin Boyer","05 65 95 10 32"),(3,"Amine Gautier","01 46 72 23 67"),(4,"Alexis Guerin","07 03 64 70 58"),
(5,"Bruno Henry","01 18 27 41 97"),(6,"Théo Lopez","07 00 19 85 49"),(7,"Nicolas Guillaume","08 66 38 67 23"),
(8,"Lamia Garcia","04 36 11 91 11"),(9,"Anaïs Cordier","01 00 15 85 05"),(10,"Florian Perrin","09 91 57 93 89");
INSERT INTO `client` (`idProject`,`fullname`,`phoneNumber`) VALUES (11,"Mathéo Huet","09 28 53 33 63"),
(12,"Guillemette Denis","01 85 72 77 05"),(13,"Antoine Clement","01 66 82 39 66"),(14,"Solene Jacquet","07 45 19 10 85"),
(15,"Lena Berger","04 13 22 81 50"),(16,"Florentin Roche","05 74 30 72 09"),(17,"Thomas Poulain","09 84 23 09 38"),
(18,"Élise Martinez","04 39 56 62 48"),(19,"Mélissa Dubois","02 36 00 17 63"),(20,"Lisa Perrot","03 63 66 14 07");


INSERT INTO `toolByProject` (`idTool`,`idProject`) VALUES (14,1),(14,2),(9,3),(15,4),(10,5),(4,6),(4,7),(2,8),(21,9),(6,10);
INSERT INTO `toolByProject` (`idTool`,`idProject`) VALUES (2,11),(25,12),(11,13),(17,14),(23,15),(24,16),(6,17),(17,18),(22,19),(12,20);
INSERT INTO `toolByProject` (`idTool`,`idProject`) VALUES (9,1),(3,2),(14,3),(20,4),(12,5),(9,6),(20,7),(4,8),(23,9),(9,10);
INSERT INTO `toolByProject` (`idTool`,`idProject`) VALUES (6,11),(10,12),(26,13),(26,14),(26,15),(6,16),(20,17),(4,18),(3,19),(18,20);

insert into document (title, summary, diffusionDate) values ('Thumbelina', 'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia', '2018-03-14');
insert into document (title, summary, diffusionDate) values ('Meltdown: Days of Destruction', 'velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue', '2017-02-08');
insert into document (title, summary, diffusionDate) values ('Prairie Love', 'felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede', '2017-11-23');
insert into document (title, summary, diffusionDate) values ('In Too Deep', 'donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum', '2018-01-14');
insert into document (title, summary, diffusionDate) values ('Apartment 1303', 'quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus', '2017-09-22');
insert into document (title, summary, diffusionDate) values ('Young Cassidy', 'enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia', '2017-09-10');
insert into document (title, summary, diffusionDate) values ('The Ghosts in Our Machine', 'eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc', '2018-04-27');
insert into document (title, summary, diffusionDate) values ('No Rest for the Brave (Pas de repos pour les braves)', 'justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat', '2017-05-24');
insert into document (title, summary, diffusionDate) values ('Heartbeats (Kohtaamisia)', 'id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea', '2018-04-06');
insert into document (title, summary, diffusionDate) values ('Jönssonligan får guldfeber', 'non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed', '2017-09-11');
insert into document (title, summary, diffusionDate) values ('Aloft', 'ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', '2018-03-16');
insert into document (title, summary, diffusionDate) values ('Phantom, The', 'lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa', '2017-09-11');
insert into document (title, summary, diffusionDate) values ('Dragonfly', 'convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam', '2017-08-10');
insert into document (title, summary, diffusionDate) values ('Amazing Journey: The Story of The Who', 'nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at', '2018-01-26');
insert into document (title, summary, diffusionDate) values ('Tristan & Isolde', 'mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus', '2017-02-13');
insert into document (title, summary, diffusionDate) values ('Outside Satan (Hors Satan)', 'morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet', '2018-03-01');
insert into document (title, summary, diffusionDate) values ('Iris', 'lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', '2017-02-08');
insert into document (title, summary, diffusionDate) values ('My Boyfriend''s Back', 'nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia', '2017-12-05');
insert into document (title, summary, diffusionDate) values ('Girl Cut in Two, The (Fille coupée en deux, La)', 'blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus', '2018-03-16');
insert into document (title, summary, diffusionDate) values ('ABC Africa', 'est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis', '2017-08-05');
insert into document (title, summary, diffusionDate) values ('Man Who Could Work Miracles, The', 'sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at', '2017-02-23');
insert into document (title, summary, diffusionDate) values ('1984 (Nineteen Eighty-Four)', 'suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris', '2017-10-09');
insert into document (title, summary, diffusionDate) values ('Is Anybody There?', 'eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', '2017-03-21');
insert into document (title, summary, diffusionDate) values ('When Jews Were Funny', 'et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id', '2017-11-15');
insert into document (title, summary, diffusionDate) values ('Viva Villa!', 'nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla', '2018-01-28');
insert into document (title, summary, diffusionDate) values ('Best of Ernie and Bert, The', 'quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue', '2017-05-31');
insert into document (title, summary, diffusionDate) values ('Koch', 'congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus', '2017-08-24');
insert into document (title, summary, diffusionDate) values ('Waterloo Bridge', 'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum', '2017-12-30');
insert into document (title, summary, diffusionDate) values ('Abouna', 'mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus', '2017-09-11');
insert into document (title, summary, diffusionDate) values ('Wee Willie Winkie', 'faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus', '2017-07-28');
insert into document (title, summary, diffusionDate) values ('Come On, Rangers', 'rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam', '2018-03-14');
insert into document (title, summary, diffusionDate) values ('Lipstick', 'praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices', '2018-03-28');
insert into document (title, summary, diffusionDate) values ('Onion Field, The', 'in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum', '2018-03-07');
insert into document (title, summary, diffusionDate) values ('Mr. Baseball', 'curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed', '2017-11-01');
insert into document (title, summary, diffusionDate) values ('Cirque du Freak: The Vampire''s Assistant', 'volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus', '2017-12-24');
insert into document (title, summary, diffusionDate) values ('Vampire in Venice (Nosferatu a Venezia) (Nosferatu in Venice)', 'eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante', '2017-06-22');
insert into document (title, summary, diffusionDate) values ('Lover''s Knot', 'ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam', '2017-12-01');
insert into document (title, summary, diffusionDate) values ('M', 'proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc', '2017-11-01');
insert into document (title, summary, diffusionDate) values ('Women, The', 'condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus', '2017-08-10');
insert into document (title, summary, diffusionDate) values ('Secretariat', 'purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer', '2017-08-17');
insert into document (title, summary, diffusionDate) values ('Taking of Pelham One Two Three, The', 'ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit', '2017-03-17');
insert into document (title, summary, diffusionDate) values ('Sparrows Dance', 'congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam', '2017-09-04');
insert into document (title, summary, diffusionDate) values ('Lara Croft Tomb Raider: The Cradle of Life', 'in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu', '2018-02-14');
insert into document (title, summary, diffusionDate) values ('Fightville', 'non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus', '2017-02-25');
insert into document (title, summary, diffusionDate) values ('Affair in Trinidad', 'iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros', '2018-01-24');
insert into document (title, summary, diffusionDate) values ('Man with Two Brains, The', 'duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan', '2018-02-20');
insert into document (title, summary, diffusionDate) values ('Room Service', 'pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis', '2018-04-10');
insert into document (title, summary, diffusionDate) values ('Streetcar Named Desire, A', 'eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus', '2017-11-17');
insert into document (title, summary, diffusionDate) values ('Hong Kong Confidential (Amaya)', 'consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur', '2017-02-20');
insert into document (title, summary, diffusionDate) values ('Clockwatchers', 'eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante', '2017-04-02');
insert into document (title, summary, diffusionDate) values ('Legends of Oz: Dorothy''s Return', 'rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis', '2017-12-10');
insert into document (title, summary, diffusionDate) values ('Electric Dragon 80.000 V', 'proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque', '2017-06-10');
insert into document (title, summary, diffusionDate) values ('This Above All', 'feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium', '2017-02-09');
insert into document (title, summary, diffusionDate) values ('Hi, Mom!', 'ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac', '2018-02-21');
insert into document (title, summary, diffusionDate) values ('Buried Alive', 'ipsum dolor sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia', '2017-04-28');
insert into document (title, summary, diffusionDate) values ('About Last Night', 'rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet', '2018-02-14');
insert into document (title, summary, diffusionDate) values ('Entr''acte', 'faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue', '2017-04-14');
insert into document (title, summary, diffusionDate) values ('Out to Sea', 'quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia', '2017-02-19');
insert into document (title, summary, diffusionDate) values ('Shunning, The', 'est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit', '2018-03-11');
insert into document (title, summary, diffusionDate) values ('Church, The (Chiesa, La)', 'donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', '2017-12-08');
insert into document (title, summary, diffusionDate) values ('Evil Dead II (Dead by Dawn)', 'aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis', '2018-04-12');
insert into document (title, summary, diffusionDate) values ('Teen Spirit', 'est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices', '2017-03-24');
insert into document (title, summary, diffusionDate) values ('Body Double', 'eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra', '2017-06-25');
insert into document (title, summary, diffusionDate) values ('Pete ''n'' Tillie', 'risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl', '2017-03-21');
insert into document (title, summary, diffusionDate) values ('Le grand soir', 'phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero', '2018-01-27');
insert into document (title, summary, diffusionDate) values ('Charlie Victor Romeo', 'eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi', '2017-03-16');
insert into document (title, summary, diffusionDate) values ('Watchmen: Tales of the Black Freighter', 'nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu', '2017-10-11');
insert into document (title, summary, diffusionDate) values ('Vantage Point', 'elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget', '2017-09-30');
insert into document (title, summary, diffusionDate) values ('Amadeus', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere', '2017-04-02');
insert into document (title, summary, diffusionDate) values ('Puppet Master 4', 'tellus nulla ut erat id mauris vulputate elementum nullam varius nulla', '2017-12-22');
insert into document (title, summary, diffusionDate) values ('Love and Other Drugs', 'eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus', '2017-06-01');
insert into document (title, summary, diffusionDate) values ('Lynch', 'primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat', '2017-11-22');
insert into document (title, summary, diffusionDate) values ('My Friend Flicka', 'justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis', '2017-05-10');
insert into document (title, summary, diffusionDate) values ('Die Frau des Frisörs', 'turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec', '2017-12-31');
insert into document (title, summary, diffusionDate) values ('Girl, Positive', 'vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor', '2017-10-11');
insert into document (title, summary, diffusionDate) values ('Oldboy', 'dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla', '2017-04-02');
insert into document (title, summary, diffusionDate) values ('Map For Saturday, A', 'mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl', '2017-07-03');
insert into document (title, summary, diffusionDate) values ('Man of Tai Chi', 'lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus', '2017-07-09');
insert into document (title, summary, diffusionDate) values ('Blood Shot', 'justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing', '2017-12-13');
insert into document (title, summary, diffusionDate) values ('301, 302 (301/302)', 'interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat', '2018-03-01');
insert into document (title, summary, diffusionDate) values ('Just Before Dawn', 'nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh', '2017-10-22');
insert into document (title, summary, diffusionDate) values ('Budd Boetticher: A Man Can Do That', 'consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas', '2018-04-07');
insert into document (title, summary, diffusionDate) values ('Kingdom II, The (Riget II)', 'ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce', '2017-04-17');
insert into document (title, summary, diffusionDate) values ('Dallas', 'elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2017-07-17');
insert into document (title, summary, diffusionDate) values ('2012: Supernova', 'at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam', '2017-04-10');
insert into document (title, summary, diffusionDate) values ('Mi Amigo Hugo', 'ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at', '2018-01-26');
insert into document (title, summary, diffusionDate) values ('Capitalism: A Love Story', 'vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante', '2017-03-05');
insert into document (title, summary, diffusionDate) values ('Dinosaur', 'nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac', '2017-03-29');
insert into document (title, summary, diffusionDate) values ('Power and Terror: Noam Chomsky in Our Times', 'eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in', '2017-05-15');
insert into document (title, summary, diffusionDate) values ('Big Bad Love', 'erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra', '2018-02-22');
insert into document (title, summary, diffusionDate) values ('Ultimate Accessory,The (100% cachemire)', 'ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in', '2017-10-22');
insert into document (title, summary, diffusionDate) values ('Out in the Dark', 'aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a', '2017-05-15');
insert into document (title, summary, diffusionDate) values ('Never Take Candy from a Stranger (Never Take Sweets from a Stranger)', 'curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien', '2017-04-19');
insert into document (title, summary, diffusionDate) values ('Pulp Fiction', 'lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit', '2018-04-16');
insert into document (title, summary, diffusionDate) values ('Blablablá', 'diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis', '2018-02-20');
insert into document (title, summary, diffusionDate) values ('The Fifth Season', 'maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra', '2017-10-12');
insert into document (title, summary, diffusionDate) values ('Hard to Hold', 'dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam', '2017-06-02');
insert into document (title, summary, diffusionDate) values ('Tell', 'nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse', '2017-09-14');
insert into document (title, summary, diffusionDate) values ('Summer of ''42', 'posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis', '2017-03-10');
insert into document (title, summary, diffusionDate) values ('Direct Contact', 'orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat', '2017-04-19');
insert into document (title, summary, diffusionDate) values ('Heat, The', 'convallis nunc proin at turpis a pede posuere nonummy integer non velit donec', '2019-04-23');
insert into document (title, summary, diffusionDate) values ('Play Dirty', 'a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida', '2018-12-16');
insert into document (title, summary, diffusionDate) values ('Little Dieter Needs to Fly', 'non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit', '2018-04-14');
insert into document (title, summary, diffusionDate) values ('Unforgettable', 'in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla', '2018-04-13');
insert into document (title, summary, diffusionDate) values ('Men with Brooms', 'congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices', '2019-02-01');
insert into document (title, summary, diffusionDate) values ('Benji the Hunted', 'quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et', '2018-08-21');
insert into document (title, summary, diffusionDate) values ('Wild, Wild Planet (I criminali della galassia)', 'ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at', '2018-01-21');
insert into document (title, summary, diffusionDate) values ('Morituri', 'pede lobortis ligula sit amet eleifend pede libero quis orci', '2018-10-12');
insert into document (title, summary, diffusionDate) values ('Tora-san''s Rise and Fall (Otoko wa tsurai yo: Torajiro aiaigasa)', 'cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel', '2018-10-27');
insert into document (title, summary, diffusionDate) values ('Pink Flamingos', 'consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc', '2019-01-08');
insert into document (title, summary, diffusionDate) values ('Best Boy', 'primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel', '2019-01-20');
insert into document (title, summary, diffusionDate) values ('Avventura, L'' (Adventure, The)', 'amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2018-10-27');
insert into document (title, summary, diffusionDate) values ('The Hunger Games: Mockingjay - Part 1', 'lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla', '2018-07-26');
insert into document (title, summary, diffusionDate) values ('Blood Done Sign My Name', 'convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est', '2018-05-22');
insert into document (title, summary, diffusionDate) values ('Trouble Man', 'augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed', '2018-03-07');
insert into document (title, summary, diffusionDate) values ('Eat a Bowl of Tea', 'phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius', '2019-01-21');
insert into document (title, summary, diffusionDate) values ('Pretty Things', 'donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum', '2018-09-30');
insert into document (title, summary, diffusionDate) values ('Wonderful and Loved by All (Underbar och älskad av alla)', 'eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet', '2018-11-16');
insert into document (title, summary, diffusionDate) values ('Class of 1984', 'ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec', '2018-10-22');
insert into document (title, summary, diffusionDate) values ('Luzhin Defence, The', 'mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc', '2018-09-07');
insert into document (title, summary, diffusionDate) values ('Lynch', 'quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed', '2018-07-28');
insert into document (title, summary, diffusionDate) values ('Concert, Le', 'vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa', '2018-09-11');
insert into document (title, summary, diffusionDate) values ('Idle Class, The', 'augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit', '2019-05-12');
insert into document (title, summary, diffusionDate) values ('Junkopia', 'at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna', '2019-02-19');
insert into document (title, summary, diffusionDate) values ('Shadow Puppets', 'et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu', '2018-05-02');
insert into document (title, summary, diffusionDate) values ('Vengeance is Mine (Fukushu suruwa wareniari)', 'aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum', '2018-09-29');
insert into document (title, summary, diffusionDate) values ('Zatoichi Goes to the Fire Festival (Zatôichi abare-himatsuri) (Zatôichi 21)', 'pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam', '2018-07-27');
insert into document (title, summary, diffusionDate) values ('Dr. Jekyll and Mr. Hyde', 'vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non', '2018-11-09');
insert into document (title, summary, diffusionDate) values ('From the East (D''Est)', 'sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero', '2018-04-25');
insert into document (title, summary, diffusionDate) values ('Look Both Ways', 'in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi', '2018-08-22');
insert into document (title, summary, diffusionDate) values ('Return of the Living Dead: Rave to the Grave', 'justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper', '2019-02-08');
insert into document (title, summary, diffusionDate) values ('L!fe Happens', 'quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida', '2018-09-28');
insert into document (title, summary, diffusionDate) values ('Never Too Young to Die', 'amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus', '2019-02-21');
insert into document (title, summary, diffusionDate) values ('Blind Spot: Hitler''s Secretary (Im toten Winkel - Hitlers Sekretärin)', 'mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque', '2018-11-20');
insert into document (title, summary, diffusionDate) values ('Dance with the Devil (Perdita Durango)', 'sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium', '2018-02-26');
insert into document (title, summary, diffusionDate) values ('Coriolanus', 'pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam', '2018-11-01');
insert into document (title, summary, diffusionDate) values ('No Flesh Shall Be Spared', 'quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus', '2019-05-16');
insert into document (title, summary, diffusionDate) values ('Female Vampire (Les avaleuses) (Erotic Kill)', 'nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in', '2019-01-02');
insert into document (title, summary, diffusionDate) values ('Country Strong', 'diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac', '2018-06-28');
insert into document (title, summary, diffusionDate) values ('Days of Grace (Días de gracia) ', 'ut mauris eget massa tempor convallis nulla neque libero convallis eget', '2018-08-09');
insert into document (title, summary, diffusionDate) values ('Guest, The', 'consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec', '2018-02-22');
insert into document (title, summary, diffusionDate) values ('Greenberg', 'sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis', '2019-04-30');
insert into document (title, summary, diffusionDate) values ('Colorado Avenue', 'a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae', '2018-12-24');
insert into document (title, summary, diffusionDate) values ('Happy Feet', 'congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo', '2018-12-09');
insert into document (title, summary, diffusionDate) values ('Ernest Goes to School', 'aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum', '2019-03-28');
insert into document (title, summary, diffusionDate) values ('Desert Rats, The', 'magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio', '2019-02-10');
insert into document (title, summary, diffusionDate) values ('Outland', 'libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur', '2018-10-27');
insert into document (title, summary, diffusionDate) values ('Grace', 'enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante', '2018-04-10');
insert into document (title, summary, diffusionDate) values ('Girl on the Bridge, The (Fille sur le pont, La)', 'imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at', '2019-02-28');
insert into document (title, summary, diffusionDate) values ('Zodiac', 'at turpis a pede posuere nonummy integer non velit donec diam neque', '2019-03-29');
insert into document (title, summary, diffusionDate) values ('Sometimes a Great Notion', 'vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit', '2019-05-02');
insert into document (title, summary, diffusionDate) values ('Don Verdean', 'aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat', '2018-12-14');
insert into document (title, summary, diffusionDate) values ('Boricua''s Bond', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin', '2018-10-08');
insert into document (title, summary, diffusionDate) values ('Trilogy of Terror II', 'lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum', '2018-07-24');
insert into document (title, summary, diffusionDate) values ('Lammbock', 'magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien', '2018-04-16');
insert into document (title, summary, diffusionDate) values ('Can-Can', 'pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris', '2018-12-14');
insert into document (title, summary, diffusionDate) values ('Return to the Blue Lagoon', 'nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed', '2019-05-26');
insert into document (title, summary, diffusionDate) values ('Closer', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis', '2018-04-14');
insert into document (title, summary, diffusionDate) values ('Silent Movie', 'praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo', '2018-05-24');
insert into document (title, summary, diffusionDate) values ('The Seven-Ups', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus', '2018-11-27');
insert into document (title, summary, diffusionDate) values ('Summertime', 'interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit', '2018-03-03');
insert into document (title, summary, diffusionDate) values ('Inner Life of Martin Frost, The', 'ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est', '2018-05-02');
insert into document (title, summary, diffusionDate) values ('Two in the Wave', 'pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate', '2018-08-15');
insert into document (title, summary, diffusionDate) values ('Judith of Bethulia', 'in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut', '2019-03-30');
insert into document (title, summary, diffusionDate) values ('Beyond, The (E tu vivrai nel terrore - L''aldilà)', 'nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae', '2018-10-04');
insert into document (title, summary, diffusionDate) values ('Doppelganger (Dopperugengâ)', 'cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at', '2019-02-03');
insert into document (title, summary, diffusionDate) values ('Days Between, The (In den Tag hinein)', 'venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien', '2018-09-24');
insert into document (title, summary, diffusionDate) values ('Yves Saint Laurent', 'suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla', '2018-02-19');
insert into document (title, summary, diffusionDate) values ('Teacher''s Pet', 'ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec', '2018-01-07');
insert into document (title, summary, diffusionDate) values ('Repo Man', 'nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo', '2018-12-01');
insert into document (title, summary, diffusionDate) values ('Godspeed', 'primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis', '2019-03-17');
insert into document (title, summary, diffusionDate) values ('Captain Midnight', 'in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla', '2018-04-14');
insert into document (title, summary, diffusionDate) values ('Deathstalker', 'in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus', '2018-11-08');
insert into document (title, summary, diffusionDate) values ('Prison Terminal: The Last Days of Private Jack Hall', 'nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut', '2018-11-28');
insert into document (title, summary, diffusionDate) values ('Parallel Sons', 'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus', '2018-08-28');
insert into document (title, summary, diffusionDate) values ('The Longest Week', 'vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', '2019-04-13');
insert into document (title, summary, diffusionDate) values ('Undefeated, The', 'enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum', '2018-12-07');
insert into document (title, summary, diffusionDate) values ('Plankton', 'id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis', '2018-01-18');
insert into document (title, summary, diffusionDate) values ('Country Bears, The', 'imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu', '2019-04-22');
insert into document (title, summary, diffusionDate) values ('Go for Zucker! (Alles auf Zucker!)', 'sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis', '2018-11-23');
insert into document (title, summary, diffusionDate) values ('Restaurant', 'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent', '2019-03-19');
insert into document (title, summary, diffusionDate) values ('Intimidation', 'faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo', '2018-11-14');
insert into document (title, summary, diffusionDate) values ('Hot Pursuit', 'nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at', '2018-08-11');
insert into document (title, summary, diffusionDate) values ('Cat in Paris, A (Une vie de chat)', 'aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst', '2018-06-06');
insert into document (title, summary, diffusionDate) values ('Beat That My Heart Skipped, The (battre mon coeur s''est arrêté, De)', 'erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam', '2018-06-06');
insert into document (title, summary, diffusionDate) values ('Son of a Gun', 'in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices', '2018-12-29');
insert into document (title, summary, diffusionDate) values ('Secret', 'sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit', '2018-01-29');
insert into document (title, summary, diffusionDate) values ('Shaft''s Big Score!', 'diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', '2018-01-16');
insert into document (title, summary, diffusionDate) values ('Pornorama', 'nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum', '2018-07-02');
insert into document (title, summary, diffusionDate) values ('Mission to Mir', 'in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis', '2018-06-13');
insert into document (title, summary, diffusionDate) values ('Harry and the Hendersons', 'natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras', '2018-03-03');
insert into document (title, summary, diffusionDate) values ('Free Money', 'molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut', '2018-11-30');
insert into document (title, summary, diffusionDate) values ('American Raspberry (Prime Time) (Funny America)', 'quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla', '2019-01-06');
insert into document (title, summary, diffusionDate) values ('Blue Dahlia, The', 'erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut', '2018-02-08');
insert into document (title, summary, diffusionDate) values ('Seraphim Falls', 'aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac', '2018-10-31');
insert into document (title, summary, diffusionDate) values ('They Made Me a Criminal (I Became a Criminal) (They Made Me a Fugitive)', 'mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis', '2018-07-19');
insert into document (title, summary, diffusionDate) values ('Fortune, The', 'cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris', '2019-01-18');
insert into document (title, summary, diffusionDate) values ('Lulu in Berlin', 'arcu libero rutrum ac lobortis vel dapibus at diam nam tristique tortor eu', '2018-11-13');
insert into document (title, summary, diffusionDate) values ('Sweetwater', 'convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum', '2018-08-22');
insert into document (title, summary, diffusionDate) values ('St. George Shoots the Dragon (Sveti Georgije ubiva azdahu)', 'ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus', '2018-09-01');
insert into document (title, summary, diffusionDate) values ('Rage in Heaven', 'et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum', '2020-01-22');
insert into document (title, summary, diffusionDate) values ('Bandit Queen', 'non pretium quis lectus suspendisse potenti in eleifend quam a', '2019-08-03');
insert into document (title, summary, diffusionDate) values ('Shoot-Out at Medicine Bend', 'amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis', '2019-05-25');
insert into document (title, summary, diffusionDate) values ('Hustle & Flow', 'felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices', '2019-02-24');
insert into document (title, summary, diffusionDate) values ('Smart People', 'blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante', '2020-01-17');
insert into document (title, summary, diffusionDate) values ('Don''t Stop Believin'': Everyman''s Journey', 'id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla', '2019-11-02');
insert into document (title, summary, diffusionDate) values ('Among Us (Onder Ons)', 'integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl', '2019-02-25');
insert into document (title, summary, diffusionDate) values ('What If ...', 'pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi', '2020-01-20');
insert into document (title, summary, diffusionDate) values ('Happy Gilmore', 'convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing', '2019-10-16');
insert into document (title, summary, diffusionDate) values ('Pride of the Marines', 'quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis', '2019-05-03');
insert into document (title, summary, diffusionDate) values ('Autumn Heart, The', 'curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien', '2019-12-23');
insert into document (title, summary, diffusionDate) values ('56 Up', 'ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis', '2019-04-14');
insert into document (title, summary, diffusionDate) values ('Out of Sight', 'rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor', '2019-08-09');
insert into document (title, summary, diffusionDate) values ('Gulliver''s Travels', 'ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus', '2019-09-14');
insert into document (title, summary, diffusionDate) values ('Good Night to Die, A', 'morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie', '2019-06-24');
insert into document (title, summary, diffusionDate) values ('Unlawful Killing', 'erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing', '2019-12-10');
insert into document (title, summary, diffusionDate) values ('Bunraku', 'amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non', '2019-06-06');
insert into document (title, summary, diffusionDate) values ('Memories of Murder (Salinui chueok)', 'montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum', '2019-03-01');
insert into document (title, summary, diffusionDate) values ('Have Dreams, Will Travel', 'justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', '2019-05-11');
insert into document (title, summary, diffusionDate) values ('Death Wish 5: The Face of Death', 'primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue', '2019-08-27');
insert into document (title, summary, diffusionDate) values ('Cartoon All-Stars to the Rescue', 'tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce', '2019-06-23');
insert into document (title, summary, diffusionDate) values ('2012', 'duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis', '2019-06-21');
insert into document (title, summary, diffusionDate) values ('Monty Python''s And Now for Something Completely Different', 'pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere', '2019-08-17');
insert into document (title, summary, diffusionDate) values ('Brief Crossing (Brève traversée)', 'nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat', '2020-01-18');
insert into document (title, summary, diffusionDate) values ('Birth of the Living Dead', 'erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non', '2019-10-28');
insert into document (title, summary, diffusionDate) values ('Kandahar (Safar e Ghandehar)', 'iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat', '2020-02-03');
insert into document (title, summary, diffusionDate) values ('Opera', 'ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus', '2019-02-08');
insert into document (title, summary, diffusionDate) values ('Wonderful Crook, The (Pas si méchant que ça)', 'non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem', '2019-03-22');
insert into document (title, summary, diffusionDate) values ('In Bloom (Grzeli nateli dgeebi)', 'venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam', '2019-08-24');
insert into document (title, summary, diffusionDate) values ('Heart of a Lion (Leijonasydän)', 'vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient', '2019-07-11');
insert into document (title, summary, diffusionDate) values ('Death Hunt', 'hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo', '2019-05-01');
insert into document (title, summary, diffusionDate) values ('Vuonna 85', 'augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget', '2019-07-29');
insert into document (title, summary, diffusionDate) values ('Amazing Spider-Man, The', 'mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus', '2019-03-22');
insert into document (title, summary, diffusionDate) values ('True Stories', 'hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer', '2019-11-14');
insert into document (title, summary, diffusionDate) values ('Map of the Sounds of Tokyo', 'vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere', '2019-09-17');
insert into document (title, summary, diffusionDate) values ('Boston''s Finest', 'integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere', '2019-11-22');
insert into document (title, summary, diffusionDate) values ('Little Thief, The (La petite voleuse)', 'mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante', '2019-04-28');
insert into document (title, summary, diffusionDate) values ('Held Up', 'quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et', '2020-01-11');
insert into document (title, summary, diffusionDate) values ('Land Before Time II: The Great Valley Adventure, The', 'lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis', '2019-09-11');
insert into document (title, summary, diffusionDate) values ('Che: Part One', 'tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin', '2019-10-02');
insert into document (title, summary, diffusionDate) values ('Go West', 'habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat', '2019-10-12');
insert into document (title, summary, diffusionDate) values ('Big Bad Mama II', 'turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo', '2019-11-22');
insert into document (title, summary, diffusionDate) values ('Evan Almighty', 'urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla', '2019-05-17');
insert into document (title, summary, diffusionDate) values ('Gentle Woman, A (Une femme douce)', 'eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis', '2019-11-21');
insert into document (title, summary, diffusionDate) values ('Sleepwalkers', 'suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit', '2019-06-09');
insert into document (title, summary, diffusionDate) values ('12 Rounds 2: Reloaded', 'nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue', '2019-07-16');
insert into document (title, summary, diffusionDate) values ('Adventures of Huck Finn, The', 'odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius', '2019-10-14');
insert into document (title, summary, diffusionDate) values ('Love of Siam, The (Rak haeng Siam)', 'eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa', '2019-02-23');
insert into document (title, summary, diffusionDate) values ('Matrix Revolutions, The', 'dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus', '2019-09-19');
insert into document (title, summary, diffusionDate) values ('Desert of the Tartars, The (Deserto dei Tartari, Il)', 'tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2020-01-01');
insert into document (title, summary, diffusionDate) values ('Too Big to Fail', 'amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur', '2019-09-02');
insert into document (title, summary, diffusionDate) values ('My Friends (Amici miei)', 'montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem', '2019-08-10');
insert into document (title, summary, diffusionDate) values ('Bebe''s Kids', 'nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate', '2019-08-18');
insert into document (title, summary, diffusionDate) values ('Grand Piano', 'nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius', '2019-10-30');
insert into document (title, summary, diffusionDate) values ('Beautiful Creatures', 'venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus', '2019-07-10');
insert into document (title, summary, diffusionDate) values ('Cop Out', 'metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi', '2019-05-14');
insert into document (title, summary, diffusionDate) values ('Ben X', 'nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse', '2019-05-24');
insert into document (title, summary, diffusionDate) values ('Dante''s Inferno', 'erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis', '2019-02-23');
insert into document (title, summary, diffusionDate) values ('Sound of Music, The', 'sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt', '2019-06-16');
insert into document (title, summary, diffusionDate) values ('Boots and Saddles', 'arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus', '2019-12-16');
insert into document (title, summary, diffusionDate) values ('7 Dollars on the Red (Sette dollari sul rosso)', 'suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet', '2019-11-05');
insert into document (title, summary, diffusionDate) values ('The Spectacular Now', 'in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis', '2019-05-09');
insert into document (title, summary, diffusionDate) values ('Institute, The', 'varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices', '2020-02-01');
insert into document (title, summary, diffusionDate) values ('Safrana or Freedom of Speech (Safrana ou le droit à la parole)', 'ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio', '2019-04-17');
insert into document (title, summary, diffusionDate) values ('Seventh Cross, The', 'aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis', '2019-09-04');
insert into document (title, summary, diffusionDate) values ('Trouble in Paradise', 'sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna', '2020-02-11');
insert into document (title, summary, diffusionDate) values ('48 Hrs.', 'fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat', '2019-04-09');
insert into document (title, summary, diffusionDate) values ('Three Lives of Thomasina, The', 'tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse', '2019-04-03');
insert into document (title, summary, diffusionDate) values ('Twice-Told Tales', 'quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est', '2020-01-03');
insert into document (title, summary, diffusionDate) values ('Hip Hop Witch, Da', 'viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer', '2019-07-04');
insert into document (title, summary, diffusionDate) values ('Dead Man Walking', 'pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris', '2019-05-12');
insert into document (title, summary, diffusionDate) values ('Best Laid Plans', 'velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat', '2019-11-10');
insert into document (title, summary, diffusionDate) values ('North Country', 'lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem', '2019-06-30');
insert into document (title, summary, diffusionDate) values ('Kill Your Darlings', 'integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed', '2019-10-13');
insert into document (title, summary, diffusionDate) values ('Woman in Black, The', 'nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum', '2019-11-21');
insert into document (title, summary, diffusionDate) values ('Father Sergius (Otets Sergiy)', 'vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices', '2019-06-26');
insert into document (title, summary, diffusionDate) values ('Godfather, The', 'dapibus duis at velit eu est congue elementum in hac', '2019-09-24');
insert into document (title, summary, diffusionDate) values ('Smile', 'gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', '2019-02-06');
insert into document (title, summary, diffusionDate) values ('Clubland (a.k.a. Introducing the Dwights)', 'vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede', '2019-05-31');
insert into document (title, summary, diffusionDate) values ('Wife! Be Like a Rose! (Tsuma yo bara no yo ni)', 'blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci', '2019-02-06');
insert into document (title, summary, diffusionDate) values ('For Greater Glory: The True Story of Cristiada', 'vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat', '2019-07-03');
insert into document (title, summary, diffusionDate) values ('Method to the Madness of Jerry Lewis', 'volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo', '2019-04-21');
insert into document (title, summary, diffusionDate) values ('Merlin''s Apprentice', 'sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent', '2019-07-19');
insert into document (title, summary, diffusionDate) values ('Body of Water (Syvälle salattu)', 'sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices', '2019-12-11');
insert into document (title, summary, diffusionDate) values ('West Beirut (West Beyrouth)', 'luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa', '2019-12-06');
insert into document (title, summary, diffusionDate) values ('Shaft', 'consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam', '2019-12-04');
insert into document (title, summary, diffusionDate) values ('Joy Ride 2: Dead Ahead', 'justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat', '2019-11-10');
insert into document (title, summary, diffusionDate) values ('Time Freak', 'sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus', '2019-06-13');
insert into document (title, summary, diffusionDate) values ('Tower Heist', 'iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis', '2019-09-04');
insert into document (title, summary, diffusionDate) values ('Son''s Room, The (Stanza del figlio, La)', 'condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget', '2019-11-09');
insert into document (title, summary, diffusionDate) values ('I Am Curious (Blue) (Jag är nyfiken - en film i blått)', 'augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non', '2019-09-21');
insert into document (title, summary, diffusionDate) values ('Werewolf of London', 'nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat', '2019-09-29');
insert into document (title, summary, diffusionDate) values ('Son of Frankenstein', 'libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis', '2019-06-16');
insert into document (title, summary, diffusionDate) values ('My Kid Could Paint That', 'enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget', '2019-02-19');
insert into document (title, summary, diffusionDate) values ('Baby of Mâcon, The (a.k.a. The Baby of Macon)', 'praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem', '2019-10-23');
insert into document (title, summary, diffusionDate) values ('Smashing Pumpkins: Vieuphoria', 'amet lobortis sapien sapien non mi integer ac neque duis', '2019-06-13');
insert into document (title, summary, diffusionDate) values ('Beaufort', 'lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2019-08-29');
insert into document (title, summary, diffusionDate) values ('Far from the Madding Crowd', 'purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa', '2019-04-04');
insert into document (title, summary, diffusionDate) values ('Underdog', 'enim blandit mi in porttitor pede justo eu massa donec dapibus duis', '2019-05-13');
insert into document (title, summary, diffusionDate) values ('Caretakers, The', 'nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi', '2019-02-03');

INSERT INTO concern (idProject, idDocument) VALUES
	('1', '63'),
	('1', '74'),
	('1', '91'),
	('2', '10'),
	('2', '12'),
	('2', '75'),
	('2', '81'),
	('3', '2'),
	('3', '24'),
	('3', '55'),
	('4', '9'),
	('4', '80'),
	('5', '28'),
	('5', '78'),
	('6', '7'),
	('6', '35'),
	('6', '70'),
	('7', '3'),
	('7', '38'),
	('7', '43'),
	('7', '53'),
	('7', '97'),
	('8', '26'),
	('8', '60'),
	('9', '14'),
	('9', '25'),
	('9', '40'),
	('9', '99'),
	('9', '100'),
	('10', '6'),
	('10', '11'),
	('10', '39'),
	('10', '52'),
	('10', '62'),
	('10', '67'),
	('10', '90'),
	('12', '8'),
	('13', '69'),
	('15', '15'),
	('15', '16'),
	('16', '95'),
	('17', '4'),
	('17', '46'),
	('17', '56'),
	('17', '72'),
	('17', '87'),
	('18', '71'),
	('19', '19'),
	('19', '23'),
	('19', '34'),
	('19', '58'),
	('19', '76'),
	('19', '83'),
	('19', '88'),
	('20', '37'),
	('20', '92'),
	('20', '93');

INSERT INTO concern (idProject, idDocument) VALUES
	('2', '63'),
    ('3', '23'),
    ('4', '43'),
    ('5', '13'),
    ('6', '3'),
    ('9', '53'),
    ('10', '73'),
    ('11','83'),
    ('8', '33');
    
insert into author (idDocument, idCoworker) values (1, 16);
insert into author (idDocument, idCoworker) values (2, 32);
insert into author (idDocument, idCoworker) values (3, 24);
insert into author (idDocument, idCoworker) values (4, 32);
insert into author (idDocument, idCoworker) values (5, 30);
insert into author (idDocument, idCoworker) values (6, 21);
insert into author (idDocument, idCoworker) values (7, 30);
insert into author (idDocument, idCoworker) values (8, 24);
insert into author (idDocument, idCoworker) values (9, 15);
insert into author (idDocument, idCoworker) values (10, 34);
insert into author (idDocument, idCoworker) values (11, 33);
insert into author (idDocument, idCoworker) values (12, 20);
insert into author (idDocument, idCoworker) values (13, 19);
insert into author (idDocument, idCoworker) values (14, 25);
insert into author (idDocument, idCoworker) values (15, 10);
insert into author (idDocument, idCoworker) values (16, 19);
insert into author (idDocument, idCoworker) values (17, 22);
insert into author (idDocument, idCoworker) values (18, 32);
insert into author (idDocument, idCoworker) values (19, 17);
insert into author (idDocument, idCoworker) values (20, 34);
insert into author (idDocument, idCoworker) values (21, 23);
insert into author (idDocument, idCoworker) values (22, 24);
insert into author (idDocument, idCoworker) values (23, 20);
insert into author (idDocument, idCoworker) values (24, 11);
insert into author (idDocument, idCoworker) values (25, 33);
insert into author (idDocument, idCoworker) values (26, 17);
insert into author (idDocument, idCoworker) values (27, 33);
insert into author (idDocument, idCoworker) values (28, 10);
insert into author (idDocument, idCoworker) values (29, 16);
insert into author (idDocument, idCoworker) values (30, 29);
insert into author (idDocument, idCoworker) values (31, 10);
insert into author (idDocument, idCoworker) values (32, 18);
insert into author (idDocument, idCoworker) values (33, 18);
insert into author (idDocument, idCoworker) values (34, 18);
insert into author (idDocument, idCoworker) values (35, 11);
insert into author (idDocument, idCoworker) values (36, 20);
insert into author (idDocument, idCoworker) values (37, 12);
insert into author (idDocument, idCoworker) values (38, 19);
insert into author (idDocument, idCoworker) values (39, 18);
insert into author (idDocument, idCoworker) values (40, 29);
insert into author (idDocument, idCoworker) values (41, 30);
insert into author (idDocument, idCoworker) values (42, 22);
insert into author (idDocument, idCoworker) values (43, 12);
insert into author (idDocument, idCoworker) values (44, 28);
insert into author (idDocument, idCoworker) values (45, 33);
insert into author (idDocument, idCoworker) values (46, 34);
insert into author (idDocument, idCoworker) values (47, 34);
insert into author (idDocument, idCoworker) values (48, 32);
insert into author (idDocument, idCoworker) values (49, 16);
insert into author (idDocument, idCoworker) values (50, 27);
insert into author (idDocument, idCoworker) values (51, 32);
insert into author (idDocument, idCoworker) values (52, 24);
insert into author (idDocument, idCoworker) values (53, 27);
insert into author (idDocument, idCoworker) values (54, 19);
insert into author (idDocument, idCoworker) values (55, 17);
insert into author (idDocument, idCoworker) values (56, 24);
insert into author (idDocument, idCoworker) values (57, 16);
insert into author (idDocument, idCoworker) values (58, 11);
insert into author (idDocument, idCoworker) values (59, 30);
insert into author (idDocument, idCoworker) values (60, 18);
insert into author (idDocument, idCoworker) values (61, 30);
insert into author (idDocument, idCoworker) values (62, 31);
insert into author (idDocument, idCoworker) values (63, 20);
insert into author (idDocument, idCoworker) values (64, 23);
insert into author (idDocument, idCoworker) values (65, 21);
insert into author (idDocument, idCoworker) values (66, 21);
insert into author (idDocument, idCoworker) values (67, 31);
insert into author (idDocument, idCoworker) values (68, 24);
insert into author (idDocument, idCoworker) values (69, 10);
insert into author (idDocument, idCoworker) values (70, 22);
insert into author (idDocument, idCoworker) values (71, 34);
insert into author (idDocument, idCoworker) values (72, 16);
insert into author (idDocument, idCoworker) values (73, 23);
insert into author (idDocument, idCoworker) values (74, 34);
insert into author (idDocument, idCoworker) values (75, 15);
insert into author (idDocument, idCoworker) values (76, 33);
insert into author (idDocument, idCoworker) values (77, 28);
insert into author (idDocument, idCoworker) values (78, 16);
insert into author (idDocument, idCoworker) values (79, 11);
insert into author (idDocument, idCoworker) values (80, 29);
insert into author (idDocument, idCoworker) values (81, 20);
insert into author (idDocument, idCoworker) values (82, 28);
insert into author (idDocument, idCoworker) values (83, 27);
insert into author (idDocument, idCoworker) values (84, 14);
insert into author (idDocument, idCoworker) values (85, 30);
insert into author (idDocument, idCoworker) values (86, 29);
insert into author (idDocument, idCoworker) values (87, 17);
insert into author (idDocument, idCoworker) values (88, 13);
insert into author (idDocument, idCoworker) values (89, 32);
insert into author (idDocument, idCoworker) values (90, 23);
insert into author (idDocument, idCoworker) values (91, 24);
insert into author (idDocument, idCoworker) values (92, 17);
insert into author (idDocument, idCoworker) values (93, 22);
insert into author (idDocument, idCoworker) values (94, 15);
insert into author (idDocument, idCoworker) values (95, 24);
insert into author (idDocument, idCoworker) values (96, 21);
insert into author (idDocument, idCoworker) values (97, 34);
insert into author (idDocument, idCoworker) values (98, 31);
insert into author (idDocument, idCoworker) values (99, 17);
insert into author (idDocument, idCoworker) values (100, 14);
insert into author (idDocument, idCoworker) values (101, 14);
insert into author (idDocument, idCoworker) values (102, 29);
insert into author (idDocument, idCoworker) values (103, 11);
insert into author (idDocument, idCoworker) values (104, 16);
insert into author (idDocument, idCoworker) values (105, 34);
insert into author (idDocument, idCoworker) values (106, 15);
insert into author (idDocument, idCoworker) values (107, 17);
insert into author (idDocument, idCoworker) values (108, 31);
insert into author (idDocument, idCoworker) values (109, 22);
insert into author (idDocument, idCoworker) values (110, 27);
insert into author (idDocument, idCoworker) values (111, 13);
insert into author (idDocument, idCoworker) values (112, 32);
insert into author (idDocument, idCoworker) values (113, 11);
insert into author (idDocument, idCoworker) values (114, 25);
insert into author (idDocument, idCoworker) values (115, 27);
insert into author (idDocument, idCoworker) values (116, 28);
insert into author (idDocument, idCoworker) values (117, 29);
insert into author (idDocument, idCoworker) values (118, 29);
insert into author (idDocument, idCoworker) values (119, 28);
insert into author (idDocument, idCoworker) values (120, 13);
insert into author (idDocument, idCoworker) values (121, 12);
insert into author (idDocument, idCoworker) values (122, 13);
insert into author (idDocument, idCoworker) values (123, 22);
insert into author (idDocument, idCoworker) values (124, 11);
insert into author (idDocument, idCoworker) values (125, 30);
insert into author (idDocument, idCoworker) values (126, 24);
insert into author (idDocument, idCoworker) values (127, 23);
insert into author (idDocument, idCoworker) values (128, 18);
insert into author (idDocument, idCoworker) values (129, 22);
insert into author (idDocument, idCoworker) values (130, 17);
insert into author (idDocument, idCoworker) values (131, 28);
insert into author (idDocument, idCoworker) values (132, 34);
insert into author (idDocument, idCoworker) values (133, 23);
insert into author (idDocument, idCoworker) values (134, 13);
insert into author (idDocument, idCoworker) values (135, 10);
insert into author (idDocument, idCoworker) values (136, 19);
insert into author (idDocument, idCoworker) values (137, 11);
insert into author (idDocument, idCoworker) values (138, 30);
insert into author (idDocument, idCoworker) values (139, 10);
insert into author (idDocument, idCoworker) values (140, 34);
insert into author (idDocument, idCoworker) values (141, 22);
insert into author (idDocument, idCoworker) values (142, 34);
insert into author (idDocument, idCoworker) values (143, 27);
insert into author (idDocument, idCoworker) values (144, 14);
insert into author (idDocument, idCoworker) values (145, 22);
insert into author (idDocument, idCoworker) values (146, 17);
insert into author (idDocument, idCoworker) values (147, 30);
insert into author (idDocument, idCoworker) values (148, 11);
insert into author (idDocument, idCoworker) values (149, 13);
insert into author (idDocument, idCoworker) values (150, 13);
insert into author (idDocument, idCoworker) values (151, 13);
insert into author (idDocument, idCoworker) values (152, 10);
insert into author (idDocument, idCoworker) values (153, 11);
insert into author (idDocument, idCoworker) values (154, 32);
insert into author (idDocument, idCoworker) values (155, 34);
insert into author (idDocument, idCoworker) values (156, 22);
insert into author (idDocument, idCoworker) values (157, 19);
insert into author (idDocument, idCoworker) values (158, 19);
insert into author (idDocument, idCoworker) values (159, 25);
insert into author (idDocument, idCoworker) values (160, 29);
insert into author (idDocument, idCoworker) values (161, 26);
insert into author (idDocument, idCoworker) values (162, 28);
insert into author (idDocument, idCoworker) values (163, 32);
insert into author (idDocument, idCoworker) values (164, 25);
insert into author (idDocument, idCoworker) values (165, 14);
insert into author (idDocument, idCoworker) values (166, 14);
insert into author (idDocument, idCoworker) values (167, 31);
insert into author (idDocument, idCoworker) values (168, 30);
insert into author (idDocument, idCoworker) values (169, 20);
insert into author (idDocument, idCoworker) values (170, 19);
insert into author (idDocument, idCoworker) values (171, 15);
insert into author (idDocument, idCoworker) values (172, 15);
insert into author (idDocument, idCoworker) values (173, 27);
insert into author (idDocument, idCoworker) values (174, 18);
insert into author (idDocument, idCoworker) values (175, 13);
insert into author (idDocument, idCoworker) values (176, 13);
insert into author (idDocument, idCoworker) values (177, 11);
insert into author (idDocument, idCoworker) values (178, 28);
insert into author (idDocument, idCoworker) values (179, 11);
insert into author (idDocument, idCoworker) values (180, 18);
insert into author (idDocument, idCoworker) values (181, 32);
insert into author (idDocument, idCoworker) values (182, 12);
insert into author (idDocument, idCoworker) values (183, 25);
insert into author (idDocument, idCoworker) values (184, 29);
insert into author (idDocument, idCoworker) values (185, 27);
insert into author (idDocument, idCoworker) values (186, 16);
insert into author (idDocument, idCoworker) values (187, 16);
insert into author (idDocument, idCoworker) values (188, 27);
insert into author (idDocument, idCoworker) values (189, 19);
insert into author (idDocument, idCoworker) values (190, 20);
insert into author (idDocument, idCoworker) values (191, 16);
insert into author (idDocument, idCoworker) values (192, 29);
insert into author (idDocument, idCoworker) values (193, 33);
insert into author (idDocument, idCoworker) values (194, 28);
insert into author (idDocument, idCoworker) values (195, 24);
insert into author (idDocument, idCoworker) values (196, 25);
insert into author (idDocument, idCoworker) values (197, 12);
insert into author (idDocument, idCoworker) values (198, 26);
insert into author (idDocument, idCoworker) values (199, 24);
insert into author (idDocument, idCoworker) values (200, 34);
insert into author (idDocument, idCoworker) values (201, 32);
insert into author (idDocument, idCoworker) values (202, 19);
insert into author (idDocument, idCoworker) values (203, 24);
insert into author (idDocument, idCoworker) values (204, 23);
insert into author (idDocument, idCoworker) values (205, 12);
insert into author (idDocument, idCoworker) values (206, 21);
insert into author (idDocument, idCoworker) values (207, 12);
insert into author (idDocument, idCoworker) values (208, 18);
insert into author (idDocument, idCoworker) values (209, 16);
insert into author (idDocument, idCoworker) values (210, 33);
insert into author (idDocument, idCoworker) values (211, 33);
insert into author (idDocument, idCoworker) values (212, 33);
insert into author (idDocument, idCoworker) values (213, 32);
insert into author (idDocument, idCoworker) values (214, 21);
insert into author (idDocument, idCoworker) values (215, 16);
insert into author (idDocument, idCoworker) values (216, 12);
insert into author (idDocument, idCoworker) values (217, 30);
insert into author (idDocument, idCoworker) values (218, 23);
insert into author (idDocument, idCoworker) values (219, 20);
insert into author (idDocument, idCoworker) values (220, 21);
insert into author (idDocument, idCoworker) values (221, 34);
insert into author (idDocument, idCoworker) values (222, 13);
insert into author (idDocument, idCoworker) values (223, 18);
insert into author (idDocument, idCoworker) values (224, 26);
insert into author (idDocument, idCoworker) values (225, 22);
insert into author (idDocument, idCoworker) values (226, 29);
insert into author (idDocument, idCoworker) values (227, 16);
insert into author (idDocument, idCoworker) values (228, 28);
insert into author (idDocument, idCoworker) values (229, 18);
insert into author (idDocument, idCoworker) values (230, 27);
insert into author (idDocument, idCoworker) values (231, 18);
insert into author (idDocument, idCoworker) values (232, 30);
insert into author (idDocument, idCoworker) values (233, 27);
insert into author (idDocument, idCoworker) values (234, 11);
insert into author (idDocument, idCoworker) values (235, 22);
insert into author (idDocument, idCoworker) values (236, 21);
insert into author (idDocument, idCoworker) values (237, 24);
insert into author (idDocument, idCoworker) values (238, 25);
insert into author (idDocument, idCoworker) values (239, 24);
insert into author (idDocument, idCoworker) values (240, 10);
insert into author (idDocument, idCoworker) values (241, 20);
insert into author (idDocument, idCoworker) values (242, 13);
insert into author (idDocument, idCoworker) values (243, 10);
insert into author (idDocument, idCoworker) values (244, 16);
insert into author (idDocument, idCoworker) values (245, 19);
insert into author (idDocument, idCoworker) values (246, 31);
insert into author (idDocument, idCoworker) values (247, 22);
insert into author (idDocument, idCoworker) values (248, 18);
insert into author (idDocument, idCoworker) values (249, 10);
insert into author (idDocument, idCoworker) values (250, 17);
insert into author (idDocument, idCoworker) values (251, 30);
insert into author (idDocument, idCoworker) values (252, 14);
insert into author (idDocument, idCoworker) values (253, 17);
insert into author (idDocument, idCoworker) values (254, 31);
insert into author (idDocument, idCoworker) values (255, 16);
insert into author (idDocument, idCoworker) values (256, 21);
insert into author (idDocument, idCoworker) values (257, 16);
insert into author (idDocument, idCoworker) values (258, 11);
insert into author (idDocument, idCoworker) values (259, 20);
insert into author (idDocument, idCoworker) values (260, 15);
insert into author (idDocument, idCoworker) values (261, 12);
insert into author (idDocument, idCoworker) values (262, 11);
insert into author (idDocument, idCoworker) values (263, 32);
insert into author (idDocument, idCoworker) values (264, 22);
insert into author (idDocument, idCoworker) values (265, 13);
insert into author (idDocument, idCoworker) values (266, 31);
insert into author (idDocument, idCoworker) values (267, 13);
insert into author (idDocument, idCoworker) values (268, 19);
insert into author (idDocument, idCoworker) values (269, 33);
insert into author (idDocument, idCoworker) values (270, 23);
insert into author (idDocument, idCoworker) values (271, 16);
insert into author (idDocument, idCoworker) values (272, 10);
insert into author (idDocument, idCoworker) values (273, 21);
insert into author (idDocument, idCoworker) values (274, 31);
insert into author (idDocument, idCoworker) values (275, 10);
insert into author (idDocument, idCoworker) values (276, 11);
insert into author (idDocument, idCoworker) values (277, 16);
insert into author (idDocument, idCoworker) values (278, 14);
insert into author (idDocument, idCoworker) values (279, 14);
insert into author (idDocument, idCoworker) values (280, 29);
insert into author (idDocument, idCoworker) values (281, 32);
insert into author (idDocument, idCoworker) values (282, 29);
insert into author (idDocument, idCoworker) values (283, 28);
insert into author (idDocument, idCoworker) values (284, 26);
insert into author (idDocument, idCoworker) values (285, 20);
insert into author (idDocument, idCoworker) values (286, 26);
insert into author (idDocument, idCoworker) values (287, 19);
insert into author (idDocument, idCoworker) values (288, 14);
insert into author (idDocument, idCoworker) values (289, 26);
insert into author (idDocument, idCoworker) values (290, 13);
insert into author (idDocument, idCoworker) values (291, 21);
insert into author (idDocument, idCoworker) values (292, 20);
insert into author (idDocument, idCoworker) values (293, 18);
insert into author (idDocument, idCoworker) values (294, 33);
insert into author (idDocument, idCoworker) values (295, 25);
insert into author (idDocument, idCoworker) values (296, 23);
insert into author (idDocument, idCoworker) values (297, 21);
insert into author (idDocument, idCoworker) values (298, 21);
insert into author (idDocument, idCoworker) values (299, 13);
insert into author (idDocument, idCoworker) values (300, 15);

INSERT INTO step (idStep, idProject, lot, technicalInformation, description) VALUES
('1', '2', '180201', NULL, 'Programmation'),
('2', '2', '180202', 'ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi', 'Tests de recette et installation'),
('3', '3', '180401', 'id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat', 'Tests de recette et installation'),
('4', '3', '180402', NULL, 'Management du projet'),
('5', '3', '180403', 'primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce', 'Gestion de configuration'),
('6', '3', '180404', 'dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo', 'Formation spécifique au projet'),
('7', '4', '191101', NULL, 'Divers'),
('8', '5', '200101', 'ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet', 'Conception'),
('9', '5', '200102', 'semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo', 'Programmation'),
('10', '5', '200103', 'pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam', 'Tests unitaires'),
('11', '5', '200104', NULL, 'Tests d\'intégration'),
('12', '5', '200105', NULL, 'Management du projet'),
('13', '6', '190901', 'pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet', 'Programmation'),
('14', '6', '190902', 'nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet', 'Tests de recette et installation'),
('15', '7', '190301', NULL, 'Programmation'),
('16', '8', '190201', 'et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien', 'Tests de recette et installation'),
('17', '8', '190202', NULL, 'Management du projet'),
('18', '8', '190203', 'vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh', 'Gestion de configuration'),
('19', '8', '190204', 'eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio', 'Formation spécifique au projet'),
('20', '9', '190401', 'mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante', 'Tests de recette et installation'),
('21', '9', '190402', NULL, 'Management du projet'),
('22', '9', '190403', 'tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor', 'Gestion de configuration'),
('23', '9', '190404', 'suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis', 'Formation spécifique au projet'),
('24', '10', '190801', NULL, 'Programmation'),
('25', '10', '190802', 'justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum', 'Tests de recette et installation'),
('26', '14', '170101', 'nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est', 'Analyse des besoins'),
('27', '14', '170102', 'nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et', 'Conception'),
('28', '14', '170103', NULL, 'Programmation'),
('29', '15', '190501', 'integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare', 'Analyse des besoins'),
('30', '16', '191301', NULL, 'Tests de recette et installation'),
('31', '16', '191302', 'id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec', 'Management du projet'),
('32', '16', '191303', NULL, 'Gestion de configuration'),
('33', '16', '191304', 'sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam', 'Formation spécifique au projet'),
('34', '17', '180301', NULL, 'Programmation'),
('35', '17', '180302', 'ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla', 'Tests de recette et installation'),
('36', '18', '190101', 'volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula', 'Conception'),
('37', '19', '191202', NULL, 'Programmation'),
('38', '19', '191203', 'penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada', 'Tests unitaires'),
('39', '19', '191204', 'ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus', 'Tests d\'intégration'),
('40', '19', '191205', 'ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse', 'Management du projet');

INSERT INTO intervention (idIntervention, idPosition, idStep, idCoworker, number, startDate, endDate, valuedCharge, validationCharge, productionCharge, technicalInformation) VALUES 
	('1', '5', '1', '5', '0001', '2018-08-16', '2018-11-16', '420', '70', '350', ''),
	('2', '5', '2', '5', '0002', '2018-11-16', '2019-02-16', '350', '35', '245', ''),
	('3', '6', '3', '6', '0003', '2018-11-28', '2018-12-01', '75', '35', '35', ''),
	('4', '7', '3', '10', '0004', '2018-12-01', '2018-12-28', '140', '5', '140', ''),
	('5', '8', '3', '18', '0005', '2018-12-28', '2019-01-01', '35', '5', '40', ''),
	('6', '5', '3', '6', '0006', '2018-10-28', '2018-11-28', '140', '5', '140', ''),
	('7', '10', '4', '28', '0007', '2019-02-01', '2019-02-28', '140', '5', '140', ''),
	('8', '3', '4', '3', '0008', '2019-01-01', '2019-02-01', '140', '5', '140', ''),
	('9', '5', '5', '7', '0009', '2019-02-28', '2019-03-01', '140', '5', '140', ''),
	('10', '6', '5', '7', '0010', '2019-03-01', '2019-03-15', '75', '35', '35', ''),
	('11', '10', '5', '29', '0011', '2019-03-15', '2019-03-28', '75', '35', '35', ''),
	('12', '3', '6', '33', '0012', '2019-03-28', '2019-04-01', '35', '35', '70', ''),
	('13', '5', '6', '5', '0013', '2019-04-01', '2019-04-28', '140', '35', '80', ''),
	('14', '3', '7', '34', '0014', '2019-11-04', '2020-05-04', '420', '70', '350', ''),
	('17', '5', '8', '8', '0017', '2020-02-01', '2020-02-15', '75', '35', '35', ''),
	('26', '8', '13', '19', '0026', '2019-10-31', '2019-11-29', '140', '5', '140', ''),
	('27', '6', '14', '8', '0027', '2019-11-29', '2019-12-30', '140', '5', '140', ''),
	('28', '5', '15', '5', '0028', '2019-05-05', '2019-11-05', '420', '70', '350', ''),
	('29', '7', '16', '11', '0029', '2019-04-02', '2019-04-15', '75', '35', '35', ''),
	('30', '8', '16', '20', '0030', '2019-04-15', '2019-04-28', '75', '35', '35', ''),
	('31', '5', '16', '8', '0031', '2019-03-13', '2019-03-30', '35', '5', '30', ''),
	('32', '6', '16', '9', '0032', '2019-03-30', '2019-04-02', '35', '45', '245', ''),
	('33', '6', '17', '6', '0033', '2019-06-16', '2019-06-27', '35', '5', '45', ''),
	('34', '10', '17', '30', '0034', '2019-07-03', '2019-07-15', '35', '35', '105', ''),
	('35', '3', '17', '3', '0035', '2019-04-28', '2019-05-12', '140', '5', '140', ''),
	('36', '4', '17', '4', '0036', '2019-05-12', '2019-05-30', '75', '35', '35', ''),
	('37', '5', '17', '5', '0037', '2019-05-30', '2019-06-16', '75', '35', '35', ''),
	('38', '6', '17', '7', '0038', '2019-06-27', '2019-07-03', '75', '35', '35', ''),
	('39', '5', '18', '7', '0039', '2019-07-15', '2019-07-31', '75', '35', '35', ''),
	('40', '6', '18', '8', '0040', '2019-07-31', '2019-08-13', '75', '35', '35', ''),
	('41', '10', '18', '31', '0041', '2019-08-13', '2019-08-31', '75', '35', '35', ''),
	('42', '6', '19', '9', '0042', '2019-08-31', '2019-09-13', '75', '35', '35', ''),
	('43', '5', '20', '9', '0043', '2019-05-12', '2019-06-12', '140', '5', '140', ''),
	('44', '10', '21', '32', '0044', '2019-06-12', '2019-07-12', '140', '5', '140', ''),
	('45', '5', '22', '5', '0045', '2019-07-12', '2019-08-12', '140', '5', '140', ''),
	('46', '10', '23', '28', '0046', '2019-10-12', '2019-11-12', '140', '5', '140', ''),
	('47', '3', '23', '33', '0047', '2019-08-12', '2019-09-12', '140', '5', '140', ''),
	('48', '5', '23', '7', '0048', '2019-09-12', '2019-10-12', '210', '5', '215', ''),
	('49', '6', '24', '6', '0049', '2019-09-24', '2019-10-24', '140', '35', '70', ''),
	('50', '7', '24', '12', '0050', '2019-10-24', '2019-11-24', '105', '35', '80', ''),
	('51', '7', '25', '13', '0051', '2019-11-24', '2019-12-24', '210', '5', '140', ''),
	('52', '11', '26', '34', '0052', '2017-07-26', '2017-08-26', '210', '15', '205', ''),
	('53', '8', '27', '21', '0053', '2017-09-26', '2017-10-26', '105', '35', '70', ''),
	('54', '5', '27', '5', '0054', '2017-08-26', '2017-09-26', '105', '35', '80', ''),
	('55', '8', '28', '22', '0055', '2017-11-26', '2018-01-26', '105', '35', '70', ''),
	('56', '5', '28', '5', '0056', '2017-10-26', '2017-11-26', '105', '35', '80', ''),
	('57', '7', '29', '14', '0057', '2019-06-12', '2019-12-12', '245', '45', '200', ''),
	('58', '8', '30', '23', '0058', '2019-12-29', '2019-12-31', '35', '5', '35', ''),
	('59', '3', '31', '34', '0059', '2019-12-31', '2020-01-10', '75', '35', '35', ''),
	('60', '6', '32', '7', '0060', '2020-01-10', '2020-01-28', '35', '5', '35', ''),
	('61', '6', '33', '8', '0061', '2020-01-28', '2020-02-12', '75', '35', '35', ''),
	('62', '6', '34', '9', '0062', '2018-10-21', '2019-02-21', '245', '45', '200', ''),
	('63', '5', '35', '5', '0063', '2019-02-21', '2019-04-21', '245', '45', '200', ''),
	('64', '6', '36', '6', '0064', '2019-01-02', '2019-07-02', '245', '45', '200', ''),
	('65', '7', '37', '15', '0065', '2019-11-19', '2019-11-25', '35', '5', '35', ''),
	('66', '8', '37', '24', '0066', '2019-11-25', '2019-11-30', '35', '5', '35', ''),
	('67', '6', '38', '7', '0067', '2019-11-30', '2019-12-02', '35', '5', '35', ''),
	('68', '7', '38', '16', '0068', '2019-12-02', '2019-12-13', '35', '5', '35', ''),
	('69', '8', '38', '25', '0069', '2019-12-13', '2019-12-22', '35', '5', '35', ''),
	('70', '6', '39', '8', '0070', '2019-12-22', '2020-01-15', '35', '5', '35', ''),
	('71', '7', '39', '17', '0071', '2020-01-15', '2019-01-30', '75', '35', '35', ''),
	('72', '4', '40', '4', '0072', '2019-01-30', '2020-02-05', '35', '5', '40', ''),
	('73', '5', '40', '9', '0073', '2020-02-05', '2020-02-16', '35', '5', '40', '');
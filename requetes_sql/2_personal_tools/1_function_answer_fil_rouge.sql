drop function if exists how_much_projects_by_society;
drop function if exists how_much_steps_interventions_documents_by_project;

delimiter $$
create function how_much_projects_by_society (pidSociety int)
returns int
not deterministic
reads sql DATA
begin
	declare rowCount INT;
	set rowCount = (SELECT count(*)
						FROM project p
						JOIN society s
						ON p.idSociety = s.idSociety
						WHERE s.idSociety = pidSociety);
    return rowCount;
end $$
delimiter ;

delimiter $$
create function how_much_steps_interventions_documents_by_project (pidProject int)
returns varchar(50)
not deterministic
reads sql DATA
begin
	declare rowCountSteps varchar(3);
    declare rowCountInterventions varchar(3);
    declare rowCountDocuments varchar(3);
    
	set rowCountSteps = (SELECT count(*)
							FROM step s
							WHERE s.idProject = pidProject);
	set rowCountInterventions = (SELECT count(*)
									FROM step s
                                    JOIN intervention i
                                    ON s.idStep = i.idStep					
									WHERE s.idProject = pidProject);
    set rowCountDocuments = (SELECT count(*)
									FROM concern c
									WHERE c.idProject = pidProject);
                                    
    return CONCAT("Steps: ", rowCountSteps,", Interventions: ",rowCountInterventions,", Documents: ", rowCountDocuments);
end $$
delimiter ;

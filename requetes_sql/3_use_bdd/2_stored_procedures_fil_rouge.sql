USE abi_fil_rouge;

drop procedure if exists average_valuedCharge_on_current_project;
drop procedure if exists found_project_by_tool_which_have_less_than_2years;
drop procedure if exists list_interventions_on_project_between_2dates;

/* CONSIGNE 12 */
/*==============================================================*/

DELIMITER $$
CREATE PROCEDURE average_valuedCharge_on_current_project ()
BEGIN
	SELECT p.codeProject AS `Numéro des projets en cours`, AVG(valuedCharge) AS `Moyenne des charges estimées` 
    FROM intervention i
	JOIN step st
    on st.idStep = i.idStep
    Join project p
    on p.idProject = st.idProject
    Join society so
    on so.idSociety = p.idSociety
    Join sector sc
    on so.idSector = sc.idSector
    WHERE p.realEndDate IS NULL
    GROUP BY p.idProject;
END$$
DELIMITER ;

/* CONSIGNE 13 */
/*==============================================================*/

DELIMITER $$
CREATE PROCEDURE found_project_by_tool_which_have_less_than_2years (ptdescription varchar(50))
BEGIN
	SELECT p.codeProject AS `Numéro des projets en cours`, t.description AS `Thème technique associé`
    FROM toolbyproject tbp
    JOIN project p
    ON p.idProject = tbp.idProject
    JOIN tool t
    ON t.idTool = tbp.idTool
    WHERE (p.realEndDate >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)) AND t.description = ptdescription;
    
END$$
DELIMITER ;

/* CONSIGNE 14 */
/*==============================================================*/

DELIMITER $$
CREATE PROCEDURE list_interventions_on_project_between_2dates (ppidProject INT, pstartDate DATE, pendDate DATE)
BEGIN
	SELECT cw.fullname AS `Nom du collaborateur`, ps.description AS `Fonction du collaborateur pendant l'intervention`,
		i.startDate AS `Intervention débutant le: `, i.endDate AS `finissant le`, st.description AS `Etape associée`
    FROM intervention i 
    JOIN step st
    ON i.idStep = st.idStep
    JOIN project p
    ON p.idProject = st.idProject
    JOIN coworker cw
    ON cw.idCoworker = i.idCoworker
    JOIN position ps
    ON i.idPosition = ps.idPosition
    WHERE (p.idProject = ppidProject) AND (i.startDate between pstartDate AND pendDate);
    
END$$
DELIMITER ;

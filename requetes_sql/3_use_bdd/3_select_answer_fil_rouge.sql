
/* CONSIGNE 1 */
/*==============================================================*/
-- Contrainte d'inclusion (supressions restreintes)
-- On ne peut pas suprimer un client s'il existe encore des projets pour ce client
/*==============================================================*/
-- Dans ma base de donnée, un client est représenté par society.
-- J'ai une contrainte de clé étrangère (on delete restricted)
-- sur la table project.  
-- J'ai créé la fonction how_much_projects_by_society (pidSociety int) 
-- pour afficher rapidement combien de projets sont associés à une société.
-- Voici un jeu de test pour répondre à cette consigne:

SELECT how_much_projects_by_society (2);
DELETE FROM society where idSociety = 2;

SELECT how_much_projects_by_society (20);
DELETE FROM society where idSociety = 20;

/* CONSIGNE 2 */
/*==============================================================*/
-- Contrainte d'inclusion (supressions en cascade)
-- La supression d'un projet entraîne la supression de toutes les étapes et interventions
-- et également de tous les documents liés à ce projet.
/*==============================================================*/
-- J'ai placé une contrainte de clé étrangère (on delete cascade)
-- sur toutes les tables concernées (intervention, step, concern, author, 
-- client (équivalent des contacts), toolByProject). 
-- Suite à la réponse du client "Un même document peut appartenir à plusieurs projets",
-- mon MCD a été modifié avec l'apparition d'une table associative: "concern" entre 
-- "project" et "document". Il n'y a donc pas de clé étrangère "project" au sein de la 
-- table "document", ce qui ne permet pas un delete on cascade des documents. 
-- J'ai donc placé un trigger after delete sur project pour supprimer un document. 
-- J'ai créé la fonction how_much_steps_interventions_documents_by_project(pidProject int) 
-- pour afficher rapidement combien d'étapes, d'interventions et de documents 
-- sont associés à un projet.
-- Voici un jeu de test pour répondre à cette consigne:

-- Affiche combien d'étape, d'intervention et de documents pour le projet n°3:
SELECT how_much_steps_interventions_documents_by_project(3);

-- Affiche la liste des documents pour le projet n°3
SELECT * FROM document d
	WHERE d.idDocument IN
		(SELECT concern.idDocument FROM concern WHERE concern.idProject = 3);

-- Affiche la liste des projets liés au document n°23
-- Les documents (3,13,23,33,43,53,63,73 et 83) sont liés à deux projets
SELECT * FROM concern c
		JOIN document d
        ON d.idDocument = c.idDocument
        Where c.idDocument = 23;

-- Supprime le projet n°3
DELETE FROM project where idProject = 3;
-- Supprime le projet n°1 et comme le document 63 n'est plus
-- lié à aucun projet, il est aussi supprimé.
DELETE FROM project where idProject = 19;

/* CONSIGNE 3 */
/*==============================================================*/
-- On souhaite obtenir par secteur d'activité, la moyenne des 
-- charges estimées des projets
/*==============================================================*/

SELECT AVG(valuedCharge) AS `Moyenne des charges estimées`, sc.description AS `Secteur d'activité` FROM intervention i
	JOIN step st
    on st.idStep = i.idStep
    Join project p
    on p.idProject = st.idProject
    Join society so
    on so.idSociety = p.idSociety
    Join sector sc
    on so.idSector = sc.idSector
    GROUP BY sc.idSector;

/* CONSIGNE 4 */
/*==============================================================*/
-- On souhaite obtenir la liste des projets (libellé court) sur lesquels
-- un collaborateur est intervenu. Préciser également sa fonction dans les projets
/*==============================================================*/

SELECT fullname, ps.description AS Fonction, shortDescription AS `Libellé court du projet`  FROM intervention i
	JOIN coworker cw
    ON i.idCoworker = cw.idCoworker
    JOIN position ps
    ON i.idPosition = ps.idPosition
    JOIN step st
    ON st.idStep = i.idStep
    JOIN project pj
    ON pj.idProject = st.idProject;

/* CONSIGNE 5 */
/*==============================================================*/
-- On souhaite obtenir à la date du jour, la liste des projets
-- en cours, par secteur d'activité.
-- Préciser le nombre de collaborateur associés aux projets et ceci par fonction
/*==============================================================*/

SELECT p.idProject, sc.description AS Secteur, COUNT(ps.description) AS `Collaborateur associés`, ps.description AS `Fonction`
	FROM project p
	JOIN society so
    ON so.idSociety = p.idSociety
    JOIN sector sc
    ON so.idSector = sc.idSector
    JOIN step st
    ON st.idProject = p.idProject
    JOIN intervention i
    ON st.idStep = i.idStep
    JOIN position ps
    ON i.idPosition = ps.idPosition
	WHERE realEndDate IS NULL
	GROUP BY p.idProject, ps.description
	ORDER BY sc.idSector;

    
/* CONSIGNE 6 */
/*==============================================================*/
-- REQUETES DE MISES A JOUR
-- On souhaite augmenter tous les salaires des collaborateurs de 
-- 5% s'ils ont plus de 5ans d'ancienneté
-- Préciser le nombre de collaborateur associés aux projets et ceci par fonction
/*==============================================================*/

-- Afficher l'historique des salaires des collaborateurs ayant 5 ans d'ancienneté avant modification
SELECT c.fullname AS `Nom du collaborateur`, s.salary AS `Salaire`, 
	earnDate `Date de la dernière révision de salaire`, 
    earnCoefficient `Coefficient d'augmentation` 
    FROM salaryhistory sh
		JOIN coworker c
		ON c.idCoworker = sh.idCoworker
		JOIN salary s
		ON s.idSalary = sh.idSalary
	WHERE (year(earnDate) < (year(curdate())-4));

UPDATE salaryhistory SET earnCoefficient = earnCoefficient+0.05, earnDate = curdate()
    WHERE (year(earnDate) < (year(curdate())-4));

-- Afficher l'historique des salaires des collaborateurs après modification
SELECT c.fullname AS `Nom du collaborateur`, s.salary AS `Salaire`, 
	earnDate `Date de la dernière révision de salaire`, 
	earnCoefficient `Coefficient d'augmentation` 
	FROM salaryhistory sh
		JOIN coworker c
		ON c.idCoworker = sh.idCoworker
		JOIN salary s
		ON s.idSalary = sh.idSalary
	WHERE (year(earnDate) = (year(curdate())))
	ORDER BY earnCoefficient DESC;

    
/* CONSIGNE 7 */
/*==============================================================*/
-- REQUETES DE MISES A JOUR
-- Supprimer de la base de données les projets qui sont terminés
-- et qui n'ont pas eut de charges (étapes) associées.
/*==============================================================*/

DELETE FROM project p
	WHERE p.realEndDate IS NOT NULL AND p.idProject NOT IN (
		SELECT p.idProject FROM step s
        WHERE s.idProject = p.idProject);
        
-- Afficher les projets qui n'ont pas d'étapes associées et qui sont terminé
SELECT p.idProject FROM project p
		LEFT JOIN step s
		ON p.idProject = s.idProject
        WHERE p.realEndDate IS NOT NULL AND s.idProject IS NULL;
        
/* CONSIGNE 8 */
/*==============================================================*/
-- TRIGGERS DE CREATION
-- Table projet: Vérifier que la date prévisionnelle de début du projet
--  est inférieure ou égale à la date prévisionnelle de fin de projet.
/*==============================================================*/

-- Insert pour vérifier le trigger:
INSERT INTO project (idProjectType, idCycle, idSociety, codeProject, shortDescription,
	 longDescription, valuedStartDate, valuedEndDate, realStartDate, realEndDate, 
	 coworkerMaxNumber, valuedCommentary, technicalInformation) VALUES
     -- inférieur
	('2', '2', '19', null, 'inter', 'venenatis tristique fusce congue diam id orn',
    '2020-02-10', '2020-02-05', '2020-02-17', null, '10', 'enim sit ametigula',
    'in porttitor pede justo eu massa donec');
    
/* CONSIGNE 9 */
/*==============================================================*/
-- TRIGGERS DE CREATION
-- Table client: Vérifier la cohérence du chiffre d'affaire du client, 
-- si suppérieur à 1 million d'euros par personne la valeur du CA est erronée.
/*==============================================================*/

-- Insert pour vérifier le trigger:
INSERT INTO society (`idNature`,`idSector`,`idType`,`number`,`businessName`,
 `adress1`, `adress2`,`city`,`postcode`,`phoneNumber`,`turnover`,`headcount`) VALUES
	(2,1,1,"9701","Enim Condimentum Eget Industries","Appartement 402",
    "7750 Dictum Avenue","Lahore","98435","01 87 77 18 40",5000000, 2);
    
/* CONSIGNE 10 */
/*==============================================================*/
-- TRIGGERS DE MISE A JOUR
-- Table personnel: Vérifier la cohérence du statut, passage possible de:
-- 		- S (stagiaire) à D (CDD) ou I (CDI)
-- 		- D (CDD) à I (CDI).
/*==============================================================*/

-- Insert un collaborateur anciennement en CDI, en Stagiaire
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(1,3,'2020-02-17','2020-07-17');

-- Insert un collaborateur anciennement en CDD, en Stagiaire
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(38,3,'2020-02-17','2020-07-17');

-- Insert un collaborateur anciennement en stage, en Stagiaire
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(51,3,'2020-02-17','2020-07-17');
    
-- Insert un collaborateur anciennement en CDD, en CDI
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(38,2,'2020-02-17',null);

-- Insert un collaborateur anciennement en stage, en CDD
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(51,1,'2020-02-17','2020-07-17');

-- Insert un collaborateur anciennement en stage, en CDI
INSERT INTO signedcontract (idCoworker,idAgreementType,agreementStartDate,agreementEndDate) VALUES
	(54,2,'2020-02-17',null);

/* CONSIGNE 11 */
/*==============================================================*/
-- TRIGGERS DE SUPRESSION
-- Table projet: Ne pas supprimer un projet si la date réelle de fin 
-- du projet est inférieure à 2 mois par rapport à la date du jour.
/*==============================================================*/

INSERT INTO project (idProjectType, idCycle, idSociety, codeProject,
shortDescription, longDescription, valuedStartDate, valuedEndDate, realStartDate, 
realEndDate, coworkerMaxNumber, valuedCommentary, technicalInformation) VALUES
('2', '2', '20', 2002, 'quis orci', 'vivamus vel nulla eget eros element', 
'2020-02-08', '2020-08-08', '2020-02-14', '2020-08-14', '18', 
'est donec odio justo sollicit', 'in blandit ultrices enim lorem'),
('2', '2', '20', 2002, 'quis orci', 'vivamus vel nulla eget eros element', 
'2020-02-08', '2020-08-08', '2020-02-14', '', '18', 
'est donec odio justo sollicit', 'in blandit ultrices enim lorem'),
('2', '2', '20', 2002, 'quis orci', 'vivamus vel nulla eget eros element', 
'2020-02-08', '2020-08-08', '2019-07-17', '2020-01-18', '18', 
'est donec odio justo sollicit', 'in blandit ultrices enim lorem');

-- Supression du projet n°19 qui n'a pas de date réelle de fin:
DELETE FROM project WHERE idProject = 19;

-- Supression du projet n°21 qui a une date de fin supérieur à la date actuelle:
DELETE FROM project WHERE idProject = 19;

-- Supression du projet n°22 qui a une donnée compromise:
DELETE FROM project WHERE idProject = 22;

-- Supression du projet n°23 qui a une date de fin de projet inférieure à 2 mois:
DELETE FROM project WHERE idProject = 23;

/* CONSIGNE 12 */
/*==============================================================*/
-- PROCEDURES STOCKEES
-- On souhaite obtenir la moyenne des charges estimées sur 
-- les projets en cours.
/*==============================================================*/

CALL average_valuedCharge_on_current_project ();

/* CONSIGNE 13 */
/*==============================================================*/
-- PROCEDURES STOCKEES
-- On souhaite obtenir sur un thème technique donné la liste des
-- projets associés et terminés depuis moins de 2ans
/*==============================================================*/

-- Affiche la liste des projets pour lesquels le thème JAVA est associé à un projet
SELECT p.codeProject AS `Numéro des projets en cours`, t.description AS `Thème technique associé`, p.realEndDate AS `Date de fin de projet`
    FROM toolbyproject tbp
    Join project p
    on p.idProject = tbp.idProject
    Join tool t
    on t.idTool = tbp.idTool
    WHERE t.description = 'JAVA';
-- Il y a trois projets, un non terminé, un passé de plus de 2ans et un de moins de 2ans.

CALL found_project_by_tool_which_have_less_than_2years ('JAVA');

/* CONSIGNE 14 */
/*==============================================================*/
-- PROCEDURES STOCKEES
-- On souhaite lister les interventions des collaborateurs sur un
-- projet entre deux dates. La procédure renvoie pour chaque
-- intervention:
-- 		- Le nom du collaborateur associé
-- 		- La fonction en claire du collaborateur
-- 		- Les dates début et fin intervention
-- 		- La tache ou activité associée
/*==============================================================*/

-- Affiche le nombre d'étapes, d'interventions et de documents pour le projet n°8
SELECT how_much_steps_interventions_documents_by_project(8);

-- Il commence le 13/03/2019 et se termine le 13/09/2019
CALL list_interventions_on_project_between_2dates(8, '2019-03-13', '2019-09-13');

-- entre deux dates inclus:
CALL list_interventions_on_project_between_2dates(8, '2019-06-13', '2019-08-13');

-- entre deux dates exclus
CALL list_interventions_on_project_between_2dates(8, '2019-02-13', '2019-10-13');
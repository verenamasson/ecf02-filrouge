drop function if exists new_project_code;

delimiter //
create function new_project_code ()
returns char(4)
not deterministic
reads sql DATA
begin
    declare current_year INT;
    declare project_rank integer;
    set current_year = YEAR(CURRENT_DATE());
    select substring(coalesce(max(`codeProject`), 0), 3) + 1
      from project
     where year(`realStartDate`) = current_year
      into project_rank;

    return CONCAT(SUBSTRING(current_year, 3), lpad(project_rank, 2, '0'));
end 
//
delimiter ;
select new_project_code();

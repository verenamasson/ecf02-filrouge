Pour démarrer la création de la base de donnée: 

    Aller dans le dossier requete_sql
    
        puis dans le dossier 1_start_bdd
            lancer les fichiers sql dans l'ordre annoncé:
                1_crebas
                2_triggers
                3_inserts
                
        aller dans le dossier 2_personal_tools
            Il s'agit de deux fonctions personnelles que j'ai créé pour faciliter
            la lecture des données. 
            Lancer le fichier 1_function_answer
            
        enfin aller dans le dossier 3_use_bdd
            lancer les fichiers sql suivant:
                1_function
                2_stored_procedures
                
            pour finir, le fichier 3_select_answer continent les réponses aux 
            consignes demandées.
            